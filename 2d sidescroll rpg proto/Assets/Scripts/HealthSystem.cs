﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public int hp;
    public int maxHP;
   
    public void changeHp(int amount)
    {
        hp += amount;
        if (hp <= 0) Die();
    }

    void Die()
    {
        Destroy(transform.gameObject);
    }
}
