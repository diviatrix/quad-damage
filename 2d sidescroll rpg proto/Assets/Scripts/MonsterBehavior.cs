﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterBehavior : MonoBehaviour
{
    public int hp;
    public int exp;
    Text text;

    private void Start() 
    {
        text = GetComponentInChildren<Text>();    
    }

    public void GetHit(int damage)
    {
        hp -= damage;
        text.GetComponent<Animator>().SetTrigger("GetAttack");
        text.text = "-"+damage;
    }

    public void Update()
    {
        if (hp <= 0) 
        {
            //Player.GetComponent<PlayerController>().AddExp(exp);
            PlayerController.playerStats.AddExp(exp);
            Die();  
        }      
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
