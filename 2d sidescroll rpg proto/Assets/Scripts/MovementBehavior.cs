﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehavior : MonoBehaviour
{
    public Vector2 moveTime;
    public float moveDistance;
    
    private Rigidbody2D rb;
    private float lastMoveTime;
    private Vector3 moveVector;

    private SpriteRenderer sprite;
    

    private void Start() 
    {
        sprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        CalculateMoveVector();
    }

    private void CalculateMoveVector()
    {
        moveVector = new Vector2 
        (
            Random.Range(-moveDistance,moveDistance),
            0
        );
        
        lastMoveTime = Time.time;
    }

    private void Update()
    {
        if(lastMoveTime + Random.Range(moveTime.x, moveTime.y) <= Time.time)
        {
            CalculateMoveVector();
            lastMoveTime = Time.time;
        }

        rb.MovePosition(transform.position + moveVector * Time.deltaTime);

        if (moveVector.x < 0){sprite.flipX = true;}
        else {sprite.flipX = false;};
    }
}
