﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState 
{
    walk,
    hold,
    interact
}

public class PlayerController : MonoBehaviour
{
    [Header("GO Settings")]
    public GameObject playerSpriteObject;
    public GameObject weaponObject;      

    [Header("game setup")]
    public bool buttonControllerEnabled;
    public float speed;
    public PlayerStats startPlayerStats;
    public static PlayerStats playerStats;
    
    [Header("Some extra info")]
    public PlayerState playerState;
    public Vector3 moveVector; 
    
    public bool weaponEnabled;
    public bool playerIsFlipped;    
    
    
    private SpriteRenderer playerSprite; 
    private Rigidbody2D rb;
    private Animator animator;   
    private bool weaponIsFlipped;

    void Start()
    {
        playerStats = startPlayerStats;
        playerStats.InitializeStats();
        
        playerIsFlipped = false;
        weaponIsFlipped = false;

        if (!weaponEnabled) DisableWeapon();
        if (buttonControllerEnabled) moveVector = Vector3.zero;

        playerSprite = playerSpriteObject.GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = playerSpriteObject.GetComponent<Animator>();
        animator.SetFloat("AxisX", 1);
    }

    public static void AddExp(int exp)
    {
        playerStats.exp += exp;
    }
    

    public void EnableWeapon()
    {
        weaponEnabled = true;
        weaponObject.SetActive(true);
    }

    public void DisableWeapon()
    {
        weaponEnabled = false;
        weaponObject.SetActive(false);
    }

    public void moveCharacter(float xValue)
    {
        moveVector.x = xValue;
    }

    // need refactor flip
    public void FlipRight()
    {
        playerIsFlipped = false;
        playerSprite.flipX = false;
        Camera.main.gameObject.GetComponent<SnapScript>().FlipCamera(playerIsFlipped);
        
        if(weaponIsFlipped)
        {
            weaponObject.transform.Rotate(Vector3.up, 180);
            weaponIsFlipped = false;
        }     
    }

    public void FlipLeft()
    {   
        playerIsFlipped = true;
        playerSprite.flipX = true;
        Camera.main.gameObject.GetComponent<SnapScript>().FlipCamera(playerIsFlipped);
        if(!weaponIsFlipped)
        {
            weaponObject.transform.Rotate(Vector3.up, 180);
            weaponIsFlipped = true;
        }      
    }

    void FixedUpdate()
    {
        UpdateMoveVector();

        if (playerState == PlayerState.walk)
        {
            UpateAnimationAndMovement();
        }  
    }

    void UpdateMoveVector()
    {
        if(buttonControllerEnabled) return;
        moveVector = Vector3.zero;
        moveVector.x = Input.GetAxisRaw("Horizontal");   
    }

    void UpateAnimationAndMovement()
    {
       if (moveVector != Vector3.zero)
        {
            MovePlayer();
            animator.SetFloat("AxisX", moveVector.x);
            animator.SetBool("isMoving", true);
        } else {
            animator.SetBool("isMoving", false);
        }
    }

    void MovePlayer()
    {
        rb.MovePosition(transform.position + moveVector.normalized * speed / 100);
    }
}
