﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerStats
{
    [Header("Health")]
    public int hp;
    public int maxHP;

    public int mana;
    public int maxMana;

    [Header("Experience")]
    public int exp;
    public int expLeft;

    public int[] exptable;

    public int level;

    [Header("Stats")]
    public int statpoints;
    
    public int Str;
    public int Agi;
    public int Vit;
    public int Int;
    public int Dex;
    public int Luk;

    public void InitializeStats()
    {
        expLeft = exptable[level];
    }

    public void AddExp(int amount)
    {
        exp += amount;
        CheckExp();
    }

    void CheckExp()
    {
        if (exp >= expLeft)
        {
            LevelUP();
        }
    }

    public void LevelUP()
    {
        level++;
        expLeft = exptable[level]; 
        statpoints += level;
        CheckExp();
    }
}
