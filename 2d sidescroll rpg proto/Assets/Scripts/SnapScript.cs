﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapScript : MonoBehaviour
{
    public GameObject snapToObject;
    public Vector3 startOffset;
    public float lerp;
    public Vector2 boundaryMax,boundaryMin;

    private Vector3 targetPosition;
    private Vector3 offset;

    private void Start() {
        offset = startOffset;
    }

    void FixedUpdate()
    {
        targetPosition = snapToObject.transform.position + offset;
        targetPosition.x = Mathf.Clamp(targetPosition.x, boundaryMin.x, boundaryMax.x );
        targetPosition.y = Mathf.Clamp(targetPosition.y, boundaryMin.y, boundaryMax.y );
        transform.position = Vector3.Lerp(transform.position, targetPosition , lerp );         
    }

    public void FlipCamera(bool isFlipped)
    {
        if (isFlipped) offset.x = -startOffset.x;
        else offset.x = startOffset.x;        
    }
}
