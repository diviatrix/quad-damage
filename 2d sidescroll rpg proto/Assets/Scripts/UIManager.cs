﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Player Settings")]
    public Text hpText;
    public Text manaText;
    public Text expText;
    public Text levelText;

    // Update is called once per frame
    void Update()
    {
        hpText.text = "HP  : " + PlayerController.playerStats.hp + "/" + PlayerController.playerStats.maxHP;
        manaText.text = "Mana: " + PlayerController.playerStats.mana + "/" + PlayerController.playerStats.maxMana;
        expText.text = "Exp : " + PlayerController.playerStats.exp + "/" + PlayerController.playerStats.expLeft;
        levelText.text = "Level : " + PlayerController.playerStats.level;
    }
}
