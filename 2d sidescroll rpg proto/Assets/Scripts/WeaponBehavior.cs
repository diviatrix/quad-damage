﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBehavior : MonoBehaviour
{
    public GameObject weaponPrefab;

    private WeaponEntity weapon;
    private bool isAttacking;
    private Animator animator;
    private float lastAttack;
    private Collider2D target;

    

    private void Start() 
    {
        InstantiateWeapon();
        animator = weapon.GetComponent<Animator>();        
    }
    void OnEnable()
    {
        lastAttack = Time.time;  
    }

    void InstantiateWeapon()
    {
        weapon = Instantiate(weaponPrefab, transform.position, Quaternion.identity, transform).GetComponent<WeaponEntity>();
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.GetComponent<MonsterBehavior>() == null) return;
        target = other;
        animator.SetBool("isAttacking", true);             
        isAttacking = true;
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.GetComponent<MonsterBehavior>() == null) return;
        
        target = null;
        animator.SetBool("isAttacking", false);   
        isAttacking = false;
    }

    private void OnTriggerStay2D(Collider2D target) 
    {
        Attack();
    }


    void Attack()
    {
        if (!isAttacking) return;
        
        if(lastAttack + weapon.cd <= Time.time)
        {
            //Debug.Log(lastAttack);
            target.GetComponent<MonsterBehavior>().GetHit(weapon.damage);
            lastAttack = Time.time; 
        }        
    }
}
