﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEntity : MonoBehaviour
{
    public int damage;
    public AnimationClip attackAnimation;
    public float cd;

    private void Start() 
    {
        cd = attackAnimation.length;
    }
}
