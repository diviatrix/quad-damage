﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectAnimationController : MonoBehaviour
{
    public bool DestroyAfterPlay;

    public AnimationClip animationClip;

    // Start is called before the first frame update
    void Start()
    {
        if (DestroyAfterPlay)
        {
            Destroy(gameObject, animationClip.length);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
