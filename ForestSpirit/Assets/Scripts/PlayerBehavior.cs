﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState 
{
    walk,
    hold,
    attack,
    interact
}

public class PlayerBehavior : MonoBehaviour
{
    [Header("game setup")]
    public bool buttonControllerEnabled;
    public float speed;
    public PlayerStats startPlayerStats;
    public static PlayerStats playerStats;

    [Header("game setup")]
    public MapEnum currentMap;

    // Start is called before the first frame update
    void OnEnable()
    {
        playerStats = startPlayerStats;
    }

    public static void AddExp(int exp)
    {
        playerStats.exp += exp;
    }
}
