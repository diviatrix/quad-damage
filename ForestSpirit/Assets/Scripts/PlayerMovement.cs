﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public GameObject playerModelGO;
    Rigidbody2D rb;
    Animator animator;

    public PlayerState playerState;

    public Vector3 moveVector; // public is debug

    void Start()
    {
        AddRigidbody2d();
        animator = playerModelGO.GetComponent<Animator>();
        playerState = PlayerState.walk;
        animator.SetFloat("AxisX", 0);
        animator.SetFloat("AxisY", -1);
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateMoveVector();

        if (playerState == PlayerState.walk)
        {
            UpateAnimationAndMovement();
        }  
    }
    
    void Update() 
    {
        if(Input.GetButtonDown("Fire1") && playerState != PlayerState.attack)
        {
            StartCoroutine(AttackCD());
        }              
    }


    // method to add rigidbody
    void AddRigidbody2d()
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        rb.freezeRotation = true;
        rb.drag = 1;
        rb.interpolation = RigidbodyInterpolation2D.Interpolate;
    }    

    void UpdateMoveVector()
    {
        moveVector = Vector3.zero;
        moveVector.x = Input.GetAxisRaw("Horizontal");
        moveVector.y = Input.GetAxisRaw("Vertical");        
    }

    private IEnumerator AttackCD()
    {
        animator.SetBool("isAttacking", true);
        playerState = PlayerState.attack;
        yield return null;      
        animator.SetBool("isAttacking", false);
        yield return new WaitForSeconds(0.2f);
        playerState = PlayerState.walk;
    }

    void UpateAnimationAndMovement()
    {
       if (moveVector != Vector3.zero)
        {
            MovePlayer();
            animator.SetFloat("AxisX", moveVector.x);
            animator.SetFloat("AxisY", moveVector.y);
            animator.SetBool("isMoving", true);
        } else {
            animator.SetBool("isMoving", false);
        }
    }

    void MovePlayer()
    {
        rb.MovePosition(transform.position + moveVector.normalized * speed / 100);
    }
}
