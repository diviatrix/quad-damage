﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapScript : MonoBehaviour
{
    public GameObject snapToObject;
    public Vector3 offset;
    public float lerp;
    public Vector2 boundaryMax,boundaryMin;
    public MapEnum currentMap;
    private Vector3 targetPosition;

    void Start()
    {
        UpdateCameraBoundaries();
    }

    public void UpdateCameraBoundaries()
    {
        switch (currentMap)
        {
            case MapEnum.Base:
                boundaryMax = new Vector2 (10,36);
                boundaryMin = new Vector2(-11f, 15);
                break;

            case MapEnum.Forest1:
                boundaryMax = new Vector2 (9.75f,9);
                boundaryMin = new Vector2(-10.7f, -11);
                break;

            case MapEnum.Forest2:
                break;

            case MapEnum.Forest3:
                break;            
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        targetPosition = snapToObject.transform.position + offset;
        targetPosition.x = Mathf.Clamp(targetPosition.x, boundaryMin.x, boundaryMax.x );
        targetPosition.y = Mathf.Clamp(targetPosition.y, boundaryMin.y, boundaryMax.y );
        transform.position = Vector3.Lerp(transform.position, targetPosition , lerp ); 
    }
}
