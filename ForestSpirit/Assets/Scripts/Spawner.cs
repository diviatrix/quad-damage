﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Spawner Settings")]
    public int maxMonstersCount;
    public float spawnCooldown;
    public float spawnRadius;
    public GameObject spawnEffect;

    [Header("Monsters")]
    public GameObject monsterPrefab;

    // privates
    private float lastSpawn;
        

    // Start is called before the first frame update
    void Start()
    {
        lastSpawn = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.childCount >= maxMonstersCount || lastSpawn+spawnCooldown > Time.time) 
        {
            return;            
        }

        else if(lastSpawn + spawnCooldown < Time.time)
        {
            GenerateMonster();
            lastSpawn = Time.time;
        }
        
    }

    void GenerateMonster()
    {
        Vector3 monsterPosition = new Vector3(
            transform.position.x * Random.Range(-spawnRadius,spawnRadius),
            transform.position.y *  Random.Range(-spawnRadius,spawnRadius),
            transform.position.z);

        SpawnEffect(monsterPosition);

        GameObject newMonster = Instantiate(monsterPrefab);
        newMonster.transform.SetParent(transform);

        newMonster.transform.position = monsterPosition;     
    }

    void SpawnEffect(Vector3 position)
    {
        position += new Vector3(0,0,-1);
        if(spawnEffect == null) return;

        Instantiate(spawnEffect).transform.position = position;
    }
}
