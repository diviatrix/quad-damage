﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHandler : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case("Transition"):
            {
                TransitionStart(col);
                return;
            }
            case("NPC"):
            {
                //NPCStart(col);
                col.GetComponent<TriggerNPC>().EnableDialog();
                return;
            }

        }
        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        switch (col.tag)
        {
            case("Transition"):
            {
                //TransitionStart(col);
                return;
            }
            case("NPC"):
            {
                //NPCStart(col);
                col.GetComponent<TriggerNPC>().DisableDialog();
                return;
            }

        }
        
    }

    void TransitionStart(Collider2D col)
    {
        TransitionTrigger mapTransition = col.GetComponent<TransitionTrigger>();        

        Camera.main.GetComponent<SnapScript>().currentMap = mapTransition.moveMap;        
        Camera.main.GetComponent<SnapScript>().UpdateCameraBoundaries();

        transform.position = mapTransition.moveTransform.position;   
    }

    void NPCStart(Collider2D col)
    {
        TriggerNPC triggerNPC = col.GetComponent<TriggerNPC>();
        triggerNPC.StartQuest();
    }
}
