﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Player Settings")]
    public Text hpText;
    public Text manaText;
    public Text expText;
    public Text levelText;
    
    // Update is called once per frame
    void Update()
    {
        PlayerStats stats = PlayerBehavior.playerStats;
        hpText.text = stats.hp.ToString();
        manaText.text = stats.mana.ToString();
        //expText.text = "Exp : " + stats.exp + "/" + stats.expLeft;
        //levelText.text = "Level : " + stats.level;
    }
    
}
