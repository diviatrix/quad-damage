﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionTrigger : MonoBehaviour
{
    public Transform moveTransform;
    public MapEnum moveMap;

    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
