﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Quest
{
    public string questName;
    public List<string> dialogs;

}

public class TriggerNPC : MonoBehaviour
{
    public GameObject dialogPrefab;
    public Quest currentQuest;
    public int currentQuestStep;
    public Quest[] quest;

    GameObject dialogGO;

    // Start is called before the first frame update
    void Start()
    {
        dialogGO = Instantiate(
            dialogPrefab, 
            new Vector3(transform.position.x, transform.position.y + 2.5f, transform.position.y), 
            transform.rotation);

        dialogGO.transform.SetParent(transform);
        
        DisableDialog();
    }

    public void EnableDialog()
    {
        dialogGO.SetActive(true);
    }

    public void DisableDialog()
    {
        dialogGO.SetActive(false);
    }

    public void StartQuest()
    {

    }
}
