﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class hpBar : MonoBehaviour
{
    
    public Text text;
    private HealthSystem healthSystem;
    private int hp,maxhp;


    void Start()
    { 
        healthSystem = GetComponent<HealthSystem>();
    }

    void Update()
    {
        
        hp = healthSystem.hp;
        maxhp = healthSystem.maxHP;
        text.text = "HP: " + hp + "/" + maxhp;
    }
}
