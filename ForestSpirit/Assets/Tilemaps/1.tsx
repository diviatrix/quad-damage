<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.0" name="1" tilewidth="32" tileheight="32" tilecount="1024" columns="32">
 <image source="../Textures/terrain_3.png" width="1024" height="1024"/>
 <terraintypes>
  <terrain name="Path" tile="354"/>
 </terraintypes>
 <tile id="1" terrain=",,,0"/>
 <tile id="2" terrain=",,0,"/>
 <tile id="33" terrain=",0,,"/>
 <tile id="34" terrain="0,,,"/>
 <tile id="64" terrain=",,,0"/>
 <tile id="65" terrain=",,0,0"/>
 <tile id="66" terrain=",,0,"/>
 <tile id="96" terrain=",0,,0"/>
 <tile id="97" terrain="0,0,0,0"/>
 <tile id="98" terrain="0,,0,"/>
 <tile id="128" terrain=",0,,"/>
 <tile id="129" terrain="0,0,,"/>
 <tile id="130" terrain="0,,,"/>
</tileset>
