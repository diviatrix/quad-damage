﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct UIEffectData
{
    public TileEnum Effect;
    public Sprite sprite;
}

public class BubbleTile : Singleton<BubbleTile>
{
    public UIEffectData[] effects;
    Animator animator;
    public Image image;
    public Text text;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void PlayPopup(TileEnum type)
    {

        int selectionCount = BattleController.Instance.selection.Count;

        image.sprite = getSprite(type);

        switch (type)
        {
            
            case TileEnum.Sword:
                {
                    text.text = "Physic Attack + " + selectionCount;
                    break;
                }
            case TileEnum.Fruit:
                {
                    text.text = "Mana regeneration + " + selectionCount;
                    break;
                }
            case TileEnum.Heart:
                {
                    if (Player.Instance.stats.hp.value < selectionCount)
                    {
                        text.text = "Health regeneration + " + selectionCount;
                    } 
                    else text.text = "You've got full hp";
                    break;
                }
            case TileEnum.Magic:
                {
                    text.text = "Magic attack + " + selectionCount;
                    break;
                }
            case TileEnum.Orange:
                {
                    text.text = "Attack boost!";
                    break;
                }
            case TileEnum.Shadow:
                {
                    text.text = "Magic attack + " + selectionCount;
                    break;
                }
            case TileEnum.Shield:
                {
                    text.text = "Shield + " + selectionCount + " points";
                    break;
                }
        }


        animator.SetTrigger("play");


    }

    Sprite getSprite(TileEnum type)
    {
        Sprite newSprite = null;

        foreach (UIEffectData data in effects)
        {
            if (data.Effect == type)
            {
                newSprite = data.sprite;
            }            
        }

        return newSprite;
    }
}
