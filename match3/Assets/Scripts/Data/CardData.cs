﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CardData : MonoBehaviour
{
    public Text monsterName;
    public Image monsterImage;
    public Text monsterHP;
    public Text monsterDamage;
    public Text monsterAtkRate;
    public Text monsterGold;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
