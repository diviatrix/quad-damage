﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyClass : MonoBehaviour
{
    public int id;

    public bool isBounty;

    [Header("Prefabs")]
    public GameObject DeathPrefab;
    public Sprite levelInfoSprite;

    [Header("Default Battle Stats")]
    public int maxHp;    
    
    public int attack_damage;    
    public float attack_delay;

    public string monsterName;

    [Header("Game Stats")]
    public int price;

    [Header("Current Battle Stats")]
    public int hp;

    [Header("Extra skills")]
    public List<SimpleEvent> skills;

    // privates
    Animator animator;

    [HideInInspector]
    public float lastAttackTime;
    bool dying = false;

    void Start()
    {
        UpdateLastAttackTime();
        animator = GetComponent<Animator>();
    }

    public void UpdateLastAttackTime()
    {
        lastAttackTime = Time.time;
    }

    public void SetHp()
    {
        hp = maxHp;
    }
    void modifyhp(int i)
    {
        hp += i;
    }

    public void GetDamage(int i)
    {
        animator.SetTrigger("getdamage");
        modifyhp(-i);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!BattleController.Instance.canControl) return;
        if (dying) return;

        if (hp <= 0)
        {
            StartCoroutine(Die());
        }

        if (lastAttackTime + attack_delay <= Time.time)
        {
            Attack();
        }

    }

    void Attack()
    {
        animator.SetTrigger("attack");
        AudioController.Instance.PlayAtkSound();
        UpdateLastAttackTime();
        Player.Instance.GetDamage(attack_damage);
    }

    IEnumerator Die()
    {        
        dying = true;
        Parallax.Instance.StartParallax();

        CollectionManager.Instance.Open(this);
        CollectionManager.Instance.Initialize();

        Instantiate(DeathPrefab, transform.position, transform.rotation);

        GetReward();
       

        yield return new WaitForSeconds(1);

        EnemyController.Instance.enemyQueue.RemoveAt(0);
        EnemyController.Instance.enemyObject = null;

                
        Destroy(gameObject);          
    }

    void GetReward() // todo:
    {
        IngameLevelsData data = GameDataController.Instance.data.ingameLevelsData;

        
        if (data.GetLevelByID(data.currentLevel) == null)
        {
            Debug.LogWarning("Failed to get reward, level doesn't exist in data");
            return;
        }

        if (isBounty && data.GetLevelByID(data.currentLevel).bountyRecieved)
        {
            Debug.Log("Bounty Already recieved");
            return;
        }

        if (isBounty && !data.GetLevelByID(data.currentLevel).bountyRecieved)
        {
            GameDataController.Instance.ChangeBalance(price);
            PanelController.Instance.PlayCoinAnimation(price);
            LevelManager.Instance.GetLevelByID(data.currentLevel).bountyRecieved = true;
            return;
        }
        

        if (!isBounty)
        {
            GameDataController.Instance.ChangeBalance(price);
            PanelController.Instance.PlayCoinAnimation(price);
        }
        else
        {
            GameDataController.Instance.ChangeBalance(price);
            PanelController.Instance.PlayCoinAnimation(price);
        }

    }
}
