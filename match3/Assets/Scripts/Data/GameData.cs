﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GameData
{  
    public bool autosync;
    public string email;
    public int version;
    public PlayerStats basicStats;
    public AudioData audioData;
    public IngameLevelsData ingameLevelsData;
    public BattleData battleData;
    public CollectionData collectionData;

    public static GameData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<GameData>(jsonString);
    }
}