﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : ClickHandler
{
    [Header("Objects")]
    public Image levelIcon;
    public Text levelName;

    public void Start()
    {        
        OnInitialize(this);
    }

    public override void OnInitialize(LevelButton levelButton)
    {        
        base.OnInitialize(levelButton);
    }

    public override void OnClick()
    {
        base.OnClick();
    }
}
