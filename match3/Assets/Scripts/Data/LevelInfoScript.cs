﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInfoScript : Singleton<LevelInfoScript>
{
    [Header("Basic Settings")]
    public Text soText;
    public int level_id;

    [Header("Monster links")]
    public Image basicMonsterImage;

    [Header("Bounty links")]
    public Text bountyText;


    [Header("Debug")]
    public List<Image> monsterImages;

    [Header("Privates")]
    private LevelSO currentSO;
    private int currentID;

    public void ShowWithId(int id)
    {
        Debug.Log($"OpenPopupWithLevel: {id}.");
        
        PanelController.Instance.OpenLevelInfoPopup();
        UpdateData(id);
    }

    
    void UpdateData(int id)
    {
        level_id = id;

        LevelSO levelSO = LevelManager.Instance.GetLevelByID(id).so;

        SetMonsters(levelSO);
        SetBounty(levelSO);

        soText.text = levelSO.Level_Settings.text;
    }
    


    public void StartGame()
    {
        LevelManager.Instance.StartLevel(currentID);
    }

    void DestroyMonsterImages()
    {
        if (monsterImages == null) return;

        foreach (Image image in monsterImages) Destroy(image.gameObject);
        monsterImages = new List<Image>();
    }
    
    void SetMonsters(LevelSO levelSO)
    {
        DestroyMonsterImages();
        basicMonsterImage.gameObject.SetActive(false);

        //Rect basicRect = basicMonsterImage.rectTransform.rect;
        int i = 0;
        foreach (EnemyInLevel enemy in levelSO.enemies)
        {
            Image temp = Instantiate(basicMonsterImage, basicMonsterImage.transform.parent);
            temp.gameObject.SetActive(true);
            temp.sprite = enemy.EnemyPrefab.GetComponent<EnemyClass>().levelInfoSprite;
            temp.GetComponentInChildren<Text>().text = "x " + enemy.amount.ToString();
            monsterImages.Add(temp);
        }

        foreach (Image img in monsterImages)
        {
            img.transform.localPosition = new Vector2(img.transform.localPosition.x + i++ * img.GetComponent<RectTransform>().rect.width * 2, img.transform.localPosition.y);
        }
    }

    void SetBounty(LevelSO levelSO)
    {
        List<EnemyInLevel> lvl = levelSO.enemies;
        currentSO = levelSO;
        currentID = levelSO.Level_Settings.id;

        int bonus = 0;



        if (GameDataController.Instance.data.ingameLevelsData.levels.Count > level_id)
        {
            if (GameDataController.Instance.data.ingameLevelsData.GetLevelByID(level_id).open)
            {
                foreach (EnemyInLevel enemy in lvl)
                {
                    if (enemy.EnemyPrefab.GetComponent<EnemyClass>().isBounty)
                    {
                        bonus += enemy.amount * enemy.EnemyPrefab.GetComponent<EnemyClass>().price;
                    }
                }

                bountyText.text = "Bounty of <b><color=orange>" + bonus + "</color></b> gold is enabled for this round";
            }
            else
            {
                foreach (EnemyInLevel enemy in lvl)
                {
                    if (!enemy.EnemyPrefab.GetComponent<EnemyClass>().isBounty)
                    {
                        bonus += enemy.amount * enemy.EnemyPrefab.GetComponent<EnemyClass>().price;
                    }
                }

                bountyText.text = "No bounty for this round, you will get <b><color=orange>" + bonus + "</color></b> gold from monsters";
            }
        }
    }
    

}
