﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerInfoScript : MonoBehaviour
{
    public Text email;
    public Text name;
    public Image profileImage;
    public Firebase.Auth.FirebaseUser user;
    public GameObject loginBlock;
    public GameObject editButton;
    public Toggle autosync;


    void Awake()
    {
        FirebaseManager.OnAuth += OnAuth;
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDestroy()
    {
        FirebaseManager.OnAuth -= OnAuth;
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    public void SwitchAutosync()
    {        
        if (autosync.isOn)
        {
            PlayerPrefs.SetInt("autosync", 1);
        }
        else PlayerPrefs.SetInt("autosync", 0);

        Debug.LogWarning(PlayerPrefs.GetInt("autosync"));
    }

    void OnLoadGameData()
    {     
        StartCoroutine(UpdatePfofileUi());
    }

    void OnAuth()
    {
        user = FirebaseManager.Instance.user;

        StartCoroutine(UpdatePfofileUi());
    }

    IEnumerator CheckLoginForm()
    {
        yield return new WaitForSeconds(.1f);

        if (user != null)
        {
            editButton.SetActive(true);
            loginBlock.SetActive(false);
        }
        else
        {
            editButton.SetActive(false);
            loginBlock.SetActive(true);
        }
    }

    IEnumerator UpdatePfofileUi()
    {
        yield return new WaitForSeconds(0.1f);

        if (user != null)
        {
            email.text = user.Email.ToString();
            if (user.DisplayName != "")
            {
                name.text = user.DisplayName;
            }
            else name.text = "unnamed";
        }
        else SetLogoffTexts();

        //Canvas.ForceUpdateCanvases();
        //StartCoroutine(DonwloadImage());
        StartCoroutine(CheckLoginForm());

        if (PlayerPrefs.GetInt("autosync") == 1) autosync.isOn = true;
        else autosync.isOn = false;
    }

    public void SetLogoffTexts()
    {
        if (user != null) return;

        string text = "log in to see";

        email.text = "Email: " + text;
        name.text = "Name: " + text;
    }

    public void ShowTop()
    {
        Application.OpenURL("market://details?id=com.ks.ForestSpirit");
    }

    public void ApplyName()
    {
        FirebaseManager.Instance.ApplyName();
        name.text = user.DisplayName;
    }

    IEnumerator DonwloadImage()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(user.PhotoUrl);
        yield return www.SendWebRequest();
        Texture2D myTexture = DownloadHandlerTexture.GetContent(www);

        Sprite sprite = Sprite.Create(myTexture, profileImage.rectTransform.rect, profileImage.rectTransform.pivot);
        profileImage.sprite = sprite;
    }    
}
