﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerStats
{
    public int gold;

    public void ChangeGold(int amount)
    {
        gold += amount;
    }

    [Header("Player Stats")]
    public PlayerStat hp;     
    public PlayerStat atk;
    public PlayerStat matk;
    public PlayerStat def;
}