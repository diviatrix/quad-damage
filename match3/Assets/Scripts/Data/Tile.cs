﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public TileData currentTileData;

    public Tile()
    {
        Randomize();
    }

    public Tile(TileData td)
    {
        currentTileData = td;
    }    

    public void Randomize()
    {
        currentTileData = TileManager.Instance.GetRandomedTileData();
    }

    public bool IsEmpty()
    {
        if (currentTileData == null) return true;
        else return false;
    }

    public void Clear()
    {
        currentTileData = null;
    }
}