﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileEnum
{
    none,
    Sword,
    Fruit,
    Heart,
    Magic,
    Orange,
    Shadow,
    Shield
}

[System.Serializable]
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TileData", order = 1)]
public class TileData : ScriptableObject
{
    public TileEnum tileEnum;
    public GameObject TilePrefab;
    public GameObject framePrefab;
    public GameObject explosionPrefab;
}


