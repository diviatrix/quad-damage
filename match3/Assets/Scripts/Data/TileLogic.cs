﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileLogic : MonoBehaviour
{
    public TileData currentTileData;
    public TileData[] tileDatas;
    GameObject frame;
    GameObject explosion;

    private void Start()
    {
        tileDatas = TileManager.Instance.tileDatas;
    }

    public void Activate()
    {
        if (frame == null)
        {
            frame = Instantiate(currentTileData.framePrefab, transform.position, Quaternion.identity);
            frame.transform.SetParent(transform);
        }

        if (explosion == null)
        {
            explosion = Instantiate(currentTileData.explosionPrefab, transform.position, Quaternion.identity);
            explosion.transform.SetParent(transform);
        }

        if (currentTileData == null)
        {
            GetComponent<SpriteRenderer>().sprite = null;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = currentTileData.TilePrefab.GetComponent<SpriteRenderer>().sprite;
        } 

        Deselect();
    }

    public TileEnum GetEnum()
    {
        if (currentTileData != null) return currentTileData.tileEnum;
        else return TileEnum.none;
    }

    public void Select()
    {
        frame.SetActive(true);
    }
    public void Deselect()
    {
        frame.SetActive(false);
    }

    public void Explode()
    {        
        explosion.GetComponent<Animator>().SetTrigger("explode");
        AudioController.Instance.PlayPopSound();
    }
}