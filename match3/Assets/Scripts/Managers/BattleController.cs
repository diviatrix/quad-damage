﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BattleData
{
    public float speed = 1;

    public float timer = 0.05f;
}

public class BattleController : Singleton<BattleController>
{
    public delegate void BattleDelegate();
    public static event BattleDelegate OnBattleWin;
    public static event BattleDelegate OnBattleStart;
    public static event BattleDelegate OnBattleLose;
    public static event BattleDelegate OnGetBounty;

    public BattleData data;
    public TextUpdate counter;

    public bool inBattle;
    public bool canControl;
    public bool inRound;
    public int matches;
    public LevelSO currentLevelSO = null;
    public List<GameObject> selection; // текущий выбор


    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void StartGameWithLevel(int id)
    {
        EmptySelection();

        inBattle = true;        


        StartNewGameWithParams(LevelManager.Instance.GetLevelByID(id).so);

        PanelController.Instance.GoToStartGame();

        OnBattleStart();
    }

    // Update is called once per frame
    void Update()
    {
        MouseControl();
        KeyboardKeys();
    }

    void Initialize()
    {
        data = GameDataController.Instance.data.battleData;
    }

    public void HandleEffect()
    {
        TileEnum td = selection[0].GetComponent<TileLogic>().currentTileData.tileEnum;

        switch (td)
        {
            case TileEnum.none:
                {
                    Debug.LogWarning("TILE ENUM IS NOT SET");
                    StartCoroutine(PopSelection());
                    break;
                }
            case TileEnum.Sword:
                {
                    Player.Instance.Attack();
                    StartCoroutine(PopSelection());
                    AudioController.Instance.PlayAtkSound();
                    break;
                }
            case TileEnum.Fruit:
                {
                    StartCoroutine(PopSelection());
                    AudioController.Instance.PlayHealSound();
                    break;
                }
            case TileEnum.Heart:
                {
                    Player.Instance.Heal(selection.Count);
                    StartCoroutine(PopSelection());
                    AudioController.Instance.PlayHealSound();
                    break;
                }
            case TileEnum.Magic:
                {

                    Player.Instance.MagicAttack();
                    StartCoroutine(PopSelection());
                    AudioController.Instance.PlayMAtkSound();
                    break;
                }
            case TileEnum.Orange:
                {
                    Player.Instance.stats.atk.value += selection.Count;
                    Player.Instance.UpdateUIwithCurrentStats();
                    AudioController.Instance.PlayHealSound();
                    StartCoroutine(PopSelection());
                    break;
                }
            case TileEnum.Shadow: // not used
                {
                    Player.Instance.MagicAttack();
                    StartCoroutine(PopSelection());
                    break;
                }
            case TileEnum.Shield:
                {
                    Player.Instance.stats.def.value += selection.Count;
                    AudioController.Instance.PlayHealSound();
                    StartCoroutine(PopSelection());
                    break;
                }
        }
        matches++;
        counter.SetText(matches);

        BubbleTile.Instance.PlayPopup(td);

    }



    public void StartNewGameWithParams(LevelSO levelSO)
    {
        //GameDataController.Instance.data.battleData.selection = new List<GameObject>();

        inRound = true;
        UnPause();

        currentLevelSO = levelSO;

        Board.Instance.FillField();
        Board.Instance.UpdateSprites();

        Player.Instance.StartRound();

        EnemyController.Instance.StartRound(levelSO.enemies);
    }

    void TryMatch()
    {
        if (selection.Count >= 3)
        {
            HandleEffect();
        }
        EmptySelection();
    }
    IEnumerator PopSelection()
    {
        float _timer = GameDataController.Instance.data.battleData.timer;

        foreach (GameObject go in selection)
        {
            Board.Instance.ExplodeTile(go);
            yield return new WaitForSeconds(_timer * GetSpeed());
        }

        yield return new WaitForSeconds(_timer * GetSpeed() * 4);

        EnemyController.Instance.EnemyHPUpdate();

        Board.Instance.ElevateAllEmptyTiles();
        Board.Instance.UpdateSprites();

        yield return new WaitForSeconds(_timer * GetSpeed());

        int iterations = 0;
        while (Board.Instance.HasEmptyTiles())
        {
            AudioController.Instance.PlaySlideSound();

            Board.Instance.FillTopRow();

            Board.Instance.UpdateSprites();
            yield return new WaitForSeconds(_timer * GetSpeed());

            Board.Instance.ElevateAllEmptyTiles();

            Board.Instance.UpdateSprites();

            yield return new WaitForSeconds(_timer * GetSpeed());

            iterations++;
            if (iterations >= Board.Instance.height * Board.Instance.width) break;
        }
    }

    void EmptySelection()
    {
        foreach (GameObject go in selection)
        {
            go.GetComponent<TileLogic>().Deselect();
        }
        selection = new List<GameObject>();
    }

    void AddGOToSelection(GameObject go)
    {
        selection.Add(go);
        go.GetComponent<TileLogic>().Select();
    }

    void HitHandle()
    {
        GameObject tileGO = HitObject();

        if (tileGO == null) return;

        if (tileGO.GetComponent<TileLogic>() == null) return; // leave if nothing clicked

        if (selection.Count == 0) // if selection is empty add first clicked tile
        {
            AddGOToSelection(tileGO);
        }

        GameObject hitTile = tileGO;
        GameObject lastTile = selection[selection.Count - 1];

        if (hitTile.GetComponent<TileLogic>().GetEnum() != lastTile.GetComponent<TileLogic>().GetEnum())
        {
            return;
            //EmptySelection();
        }
        // проверка на координаты, чтобы не дальше +1/-1 по x/y
        else if (hitTile.transform.position.x > lastTile.transform.position.x + 1 || hitTile.transform.position.x < lastTile.transform.position.x - 1)
        {
            return;
        }
        else if (hitTile.transform.position.y > lastTile.transform.position.y + 1 || hitTile.transform.position.y < lastTile.transform.position.y - 1)
        {
            return;
        }
        // проверка на добавление уже добавленного тайла в selection
        else if (HasSameTileInSelection(hitTile))
        {
            return;
        }

        // подумать как реализовать чтобы можно было в обратную сторону вести выделение и убирать тайлы из селекшена 
        /*
        else if(hitTile = lastTile)
        {
            selection.Remove(lastTile);
            lastTile.GetComponent<TileLogic>().Deselect();
            return;
        }
        */

        AddGOToSelection(tileGO);
    }

    bool HasSameTileInSelection(GameObject go)
    {
        bool b = false;
        foreach (GameObject s in selection)
        {
            if (s == go) b = true;
        }

        return b;
    }


    GameObject HitObject()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (hit.transform != null) return (hit.transform.gameObject);
        else return null;
    }

    public void Pause()
    {
        if (!inRound) return;

        canControl = false;
    }

    public void SwitchPause()
    {
        if (!inRound) return;

        canControl = !canControl;
    }

    public void UnPause()
    {
        if (!inRound) return;

        canControl = true;
        if (EnemyController.Instance.enemyObject != null) EnemyController.Instance.enemyObject.GetComponent<EnemyClass>().UpdateLastAttackTime();
    }


    float GetSpeed()
    {
        return 1 / data.speed;
    }

    void ModSpeedBy(float s)
    {
        data.speed += s;
        if (data.speed <= 0) data.speed = 0.01f;
    }

    void KeyboardKeys()
    {
        if (Input.GetKeyDown(KeyCode.PageDown)) ModSpeedBy(-0.25f);
        if (Input.GetKeyDown(KeyCode.PageUp)) ModSpeedBy(0.25f);
    }

    void MouseControl()
    {
        if (!canControl) return;

        if (Input.GetMouseButtonUp(0))
        {
            TryMatch();
        }
        if (Input.GetMouseButton(0))
        {
            HitHandle();
        }
    }

    public void WinRound()
    {
        canControl = false;
        inRound = false;

        matches = 0;

        OnBattleWin();
    }

    public void CancelRound()
    {
        canControl = false;
        inRound = false;

        Player.Instance.Lose();
        EnemyController.Instance.Wipe();
        Player.Instance.Wipe();
        matches = 0;

        PanelController.Instance.GoToMap();
    }
}
