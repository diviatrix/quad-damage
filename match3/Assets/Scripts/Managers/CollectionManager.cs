﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct CollectionCard
{
    public int id;
    public Sprite sprite;
    public string name;
    public string hp;
    public string attack;
    public string attackRate;
    public string gold;

}

[System.Serializable]
public class CollectionData
{
    public List<CollectionCard> cards;
}

[System.Serializable]
public class CollectionManager : Singleton<CollectionManager>
{
    public GameObject basicCard;
    public Transform content;

    public List<GameObject> buttons;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void Open(EnemyClass enemy)
    {
        if (GameDataController.Instance.data.collectionData.cards == null)
        {
            GameDataController.Instance.data.collectionData.cards = new List<CollectionCard>();
        }
        else        
        {
            foreach (CollectionCard card in GameDataController.Instance.data.collectionData.cards)
            {
                if (enemy.id == card.id)
                {
                    return;
                }
            }
        }

        CollectionCard newCard = new CollectionCard
        {
            id = enemy.id,
            sprite = enemy.levelInfoSprite,
            name = enemy.monsterName,
            hp = enemy.maxHp.ToString(),
            attack = enemy.attack_damage.ToString(),
            attackRate = enemy.attack_delay.ToString(),
            gold = enemy.price.ToString()
        };

        GameDataController.Instance.data.collectionData.cards.Add(newCard);
    }

    public void Initialize()
    {
        WipeButtons();

        int i = 0;
        basicCard.SetActive(false);

        if (GameDataController.Instance.data.collectionData == null)
        {
            GameDataController.Instance.data.collectionData = new CollectionData();
            return;
        };

        foreach (CollectionCard card in GameDataController.Instance.data.collectionData.cards)
        {
            GameObject temp = Instantiate(basicCard, content);
            content.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(content.gameObject.GetComponent<RectTransform>().sizeDelta.x, basicCard.GetComponent<RectTransform>().sizeDelta.y * (i+1));
            temp.SetActive(true);
            //temp.transform.Translate(Vector3.down * 75 * i++, content);
            temp.GetComponent<RectTransform>().localPosition = new Vector2(basicCard.transform.localPosition.x, basicCard.transform.localPosition.y - basicCard.GetComponent<RectTransform>().sizeDelta.y * i);
            temp.GetComponent<CardData>().monsterImage.sprite = card.sprite;
            temp.GetComponent<CardData>().monsterHP.text = "Monster HP: " + card.hp;
            temp.GetComponent<CardData>().monsterDamage.text = "Monster Attack: " + card.attack;
            temp.GetComponent<CardData>().monsterGold.text = "Gold Amount: " + card.gold;
            temp.GetComponent<CardData>().monsterAtkRate.text = "Monster Attack rate: " + card.attackRate + " s.";
            temp.GetComponent<CardData>().monsterName.text = "Monster name: <color=brown>" + card.name + "</color>";

            buttons.Add(temp);
            i++;
        }
    }

    void WipeButtons()
    {
        if (buttons != null)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                Destroy(buttons[i]);
            }
        }

        buttons = new List<GameObject>();
    }
}
