﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

[System.Serializable]
public class FirebaseManager : Singleton<FirebaseManager>
{
    public delegate void FirebaseDelegate();
    public static event FirebaseDelegate OnAuth;

    [Header("Firebase settings")]
    public string googleIdToken;
    public string googleAccessToken;

    [Header("Input Fields")]
    public InputField emailField;
    public InputField pwdField;
    public InputField nameField;
    public Text status;

    [Header("Firebase data")]
    public FirebaseUser user;
    public FirebaseUser appUser;
    Firebase.Auth.FirebaseAuth auth;
    FirebaseDatabase db;
    Firebase.FirebaseApp app;
    DatabaseReference dbReference;

    [Header("Debug")]
    public string email;
    public string password;
    public string snapshot = "";



    void Awake()
    {
        StartCoroutine(InitializeFirebase());
    }

    IEnumerator InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);

        StartCoroutine(InitializeDB());

        yield return new WaitForSeconds(0);
    }

    IEnumerator InitializeDB()
    {
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://forst-spirit-69431750.firebaseio.com/");

        // Get the root reference location of the database.
        dbReference = FirebaseDatabase.DefaultInstance.RootReference;

        db = FirebaseDatabase.DefaultInstance;

        yield return new WaitForSeconds(0.1f);
    }


    public void LoginWithGoogle()
    {
        Firebase.Auth.Credential credential =
        Firebase.Auth.GoogleAuthProvider.GetCredential(googleIdToken, googleAccessToken);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                SetStatusText("Signed out " + user.UserId);
                user = null;
            }
            user = auth.CurrentUser;

            if (signedIn)
            {
                SetStatusText("Signed in " + user.Email);                
            }
        }

        OnAuth();
    }

    void SetStatusText(String text)
    {
        status.text = text;
    }

    public void ApplyName()
    {
        if (nameField.text == "") return;

        UserProfile temp = new UserProfile();
        temp.DisplayName = nameField.text;
        temp.PhotoUrl = auth.CurrentUser.PhotoUrl;
        auth.CurrentUser.UpdateUserProfileAsync(temp);

        OnAuth();
    }

    public void LogOut()
    {
        auth.SignOut();
        OnAuth();        
    }

    public string LoadGameDataFromFirebase()
    {        

        if (user == null) return snapshot;

        db.GetReference("Users").Child(user.UserId).GetValueAsync().ContinueWith(task => 
        {
            if (task.IsFaulted)
            {
                Debug.Log("Tried to download data from firebase, but failed");
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result.Value.ToString();
            }
        }
        );

        return snapshot;
    }

    public void SaveGameDataToFirebase(GameData gd)
    {
        if (user == null) return;

        gd.email = user.Email;

        string key = dbReference.Child("Users").Push().Key;

        string userEntry = JsonUtility.ToJson(gd);

        //dbReference.Child("Users").Child(user.UserId).SetRawJsonValueAsync(userEntry);

        dbReference.Child("Users").Child(user.UserId).SetValueAsync(userEntry);
    }

    public void Register()
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                SetStatusText("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                SetStatusText("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            user = newUser;
            SetStatusText( "User created successfully:" + newUser.DisplayName + ", " + newUser.UserId);
        });
    }

    public void Login()
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => 
        {
            if (task.IsCanceled)
            {
                SetStatusText("SignInWithEmailAndPasswordAsync was canceled.");
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                SetStatusText("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            
            user = newUser;           

        });
        OnAuth();
    }
    

    public void SetPassword()
    {
        password = pwdField.text;
    }
    public void SetEmail()
    {
        email = emailField.text;
    }
}


