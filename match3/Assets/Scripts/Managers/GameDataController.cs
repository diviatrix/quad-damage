﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameDataController : Singleton<GameDataController>
{
    public delegate void GameDataDelegate();
    public static event GameDataDelegate OnLoadGameData;
    public static event GameDataDelegate OnGameDataChange;

    public bool autoload;
    public GameVarsSO defaultData;
    public GameData data;

    public List<float> SaveRequests = new List<float>();

    private void Start()
    {
        if (Application.isMobilePlatform) autoload = true;
        LoadData();
    }

    void LoadData()
    {
        if (autoload && Load())
        {
            Debug.Log("Load gameData from disk successful");
        }
        else
        {
            SetDefaultData();
        }

        OnLoadGameData();
    }

    public void ChangeBalance(int amount)
    {
        data.basicStats.ChangeGold(amount);
    }

    public void ReuqestSave()
    {
        SaveRequests.Add(Time.time);
    }

    void Save()
    {        
        SaveSystem.Instance.SaveGameToDisk(data);        
    }
    
    bool Load()
    {
        bool success = false;

        SaveData saveData = SaveSystem.Instance.LoadGameFromDisk();

        if (!saveData.isEmpty)
        {
            GameData temp = GameData.CreateFromJSON(saveData.gameData);

            data.basicStats = temp.basicStats;
            data.audioData = temp.audioData;
            data.autosync = temp.autosync;
            data.battleData = temp.battleData;
            data.collectionData = temp.collectionData;
            data.email = temp.email;
            data.ingameLevelsData.levels = temp.ingameLevelsData.levels;
            data.version = temp.version;

            success = true;
        }
        return success;
    }    

    void SetDefaultData()
    {
        data.basicStats = defaultData.DefaultStats;

        foreach (LevelSO levelSO in defaultData.DefaultLevels)
        {
            IngameLevel level = new IngameLevel(levelSO);    
            data.ingameLevelsData.levels.Add(level);
        }
        
    }

    private void FixedUpdate()
    {
        CheckSaveQueue();
    }

    void CheckSaveQueue()
    {
        if (SaveRequests.Count > 0)
        {
            if (SaveRequests[SaveRequests.Count - 1] + 3 < Time.time)
            {
                SaveRequests = new List<float>();
                Save();
            }
        }
    }

    private void OnApplicationQuit()
    {
        //Save();
    }

    /*
    
    //public bool postload;
    //public bool syncing;
    //public float autosaveTime;

    public void SaveGameDataToFirebase()
    {
        StartCoroutine(SaveToFirebase());
    }

    IEnumerator SaveToFirebase()
    {
        yield return new WaitForSeconds(1f);
        FirebaseManager.Instance.SaveGameDataToFirebase(data);
    }
    
    void Awake()
    {
        FirebaseManager.OnAuth += OnAuth;
    }
    void OnDestroy()
    {
        FirebaseManager.OnAuth -= OnAuth;
    }

    void OnAuth()
    {
        if (FirebaseManager.Instance.user == null)
        {
            SetDefaultData();
            OnLoadGameData();
            return;
        }

        LoadGameDataFromFirebase();              
    }

    private void FixedUpdate()
    {
        if (syncing) return;

        else if (PlayerPrefs.GetInt("autosync") == 0) return;

        else if (FirebaseManager.Instance.user == null) return;

        StartCoroutine(SyncWithFirebase());
    }

    IEnumerator LoadGDfromFirebase()
    {
        yield return new WaitForSeconds(0.1f);

        if (LoadFromFirebase())
        {
            Debug.Log("Successfully loaded from firebase");
        }

        OnLoadGameData();
        Save();
    }


    public void LoadGameDataFromFirebase()
    {
        StartCoroutine(LoadGDfromFirebase());
    }

    bool LoadFromFirebase()
    {
        if (FirebaseManager.Instance.user == null) return false;


        bool success = false;

        string snapshot;
        snapshot = FirebaseManager.Instance.LoadGameDataFromFirebase();

        if (snapshot == "")
        {
            return false;
        }

        GameData temp = GameData.CreateFromJSON(snapshot);
        temp.email = FirebaseManager.Instance.user.Email.ToString();

        if (temp.version == defaultData.gameData.version)
        {
            data = temp;
            success = true;
        }

        return success;
    }

    IEnumerator SyncWithFirebase ()
    {
        syncing = true;
        
        yield return new WaitForSeconds(autosaveTime);

        if (postload != true)
        {
            //LoadGameDataFromFirebase();
            postload = true;
        }

        SaveGameDataToFirebase();
        Save();

        syncing = false;
    }
    */
}
