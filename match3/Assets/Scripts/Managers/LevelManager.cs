﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.UIElements;

[System.Serializable]
public class IngameLevel
{
    public int id;
    public LevelSO so;
    public bool open;
    public bool repeatable;
    public bool bountyRecieved;

    public Sprite sprite;

    public void SetOpen(bool b)
    {
        open = b;
    }

    public IngameLevel (LevelSO levelSO)
    {
        so = levelSO;
        id = levelSO.Level_Settings.id;
        open = levelSO.Level_Settings.open;
        repeatable = levelSO.Level_Settings.repeatable;

        sprite = so.enemies[so.enemies.Count - 1].EnemyPrefab.GetComponent<EnemyClass>().levelInfoSprite;
    }

    public void OpenLevel()
    {
        open = true;
        LevelManager.Instance.ActivateLevelButton(id);
    }
}

[System.Serializable]
public class IngameLevelsData
{
    [Header("Ingame level Stats")]
    public List<IngameLevel> levels;


    [Header("Level Stats")]
    public int lastLevel;
    public int currentLevel;    

    
    public IngameLevel GetLevelByID(int id)
    {
        IngameLevel temp = null;

        foreach (IngameLevel i in levels)
        {
            if (i.id == id) temp = i;
        }

        if (temp == null)
        {
            temp = LevelManager.Instance.GetLevelByID(id);
        }

        return temp;
    }

    

    public bool isLevelOpen(int id)
    {
        return GetLevelByID(id).open;
    }

    public void AddLevel(IngameLevel ingameLevel)
    {
        if(!levels.Contains(ingameLevel)) levels.Add(ingameLevel);
    }

    public void OpenLevel(int id)
    {
        foreach (var ingameLevel in levels)
        {
            if (ingameLevel.id == id)
            {
                ingameLevel.open = true;
            }
        }
    }

    public void UpdateLastLevel()
    {
        lastLevel = currentLevel;
    }
    
}

public class LevelManager : Singleton<LevelManager>
{
    [Header("Buttons")]
    public Transform buttonContainer;
    public IngameLevelsData data;
    public List<LevelButton> levelButtons;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
        BattleController.OnBattleWin += OnBattleWin;
        BattleController.OnBattleLose += OnBattleLose;
        BattleController.OnBattleStart += OnBattleStart;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
        BattleController.OnBattleWin -= OnBattleWin;
        BattleController.OnBattleLose -= OnBattleLose;
        BattleController.OnBattleStart -= OnBattleStart;
    }

    void OnBattleWin()
    {
        WinRound();
    }
    void OnBattleLose()
    {

    }
    void OnBattleStart()
    {

    }

    public void AddButtonToList(LevelButton levelButton)
    {
        if (!levelButtons.Contains(levelButton))
        {
            levelButtons.Add(levelButton);
        }
    }

    private void OnLoadGameData()
    {
        Initialize();
    }


    public void Initialize()
    {
        data = GameDataController.Instance.data.ingameLevelsData;
        SetLevelButtons();
    }

    void SetLevelButtons()
    {
        foreach (LevelButton levelButton in levelButtons)
        {
            foreach (IngameLevel ingameLevel in data.levels)
            {
                if (ingameLevel.id == levelButton.id)
                {
                    levelButton.levelIcon.sprite = ingameLevel.sprite;
                    levelButton.levelName.text = ingameLevel.so.Level_Settings.text;
                    
                    if (!ingameLevel.open)
                    {
                        DeactivateLevelButton(levelButton.id);
                    }
                }
            }
        }
    }

    void DeactivateLevelButton(int id)
    {
        foreach (LevelButton levelButton in levelButtons)
        {
            if (levelButton.id == id)
            {
                levelButton.levelIcon.color = new Color(1, 1, 1, .5f);
                levelButton.levelName.color = new Color(1, 1, 1, .5f);
                levelButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void ActivateLevelButton(int id)
    {
        foreach (LevelButton levelButton in levelButtons)
        {
            if (levelButton.id == id)
            {
                levelButton.levelIcon.color = new Color(1, 1, 1, 1);
                levelButton.levelName.color = new Color(1, 1, 1, 1);
                levelButton.GetComponent<Button>().interactable = true;
            }
        }
    }




    public void StartLevel(int id)
    {
        GameDataController.Instance.data.ingameLevelsData.currentLevel = id;

        PanelController.Instance.GoToStartGame();

        BattleController.Instance.StartGameWithLevel(id);
    }

    public void WinRound()
    {       
        OpenLevel(data.currentLevel+1);
        GameDataController.Instance.ReuqestSave();
    }

    public void OpenLevel(int id)
    {
        if (GetLevelByID(id) == null) return;
        
        GetLevelByID(id).OpenLevel();
    }

    public IngameLevel GetLevelByID(int id)
    {
        IngameLevel temp = null;

        foreach (IngameLevel ingameLevel in data.levels)
        {
            if (ingameLevel.id == id) temp = ingameLevel;
        }

        return temp;
    }


    public void FillButtons()
    {
        foreach (Transform child in buttonContainer)
        {
            if (child.GetComponent<LevelButton>())
            {
                //child.GetComponent<LevelButton>().Initialize();
            }
        }
    }
}
