﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEditor;
using UnityEngine;

public class Parallax : Singleton<Parallax>
{
    public float offset;
    public bool pause;
    float speed = 1;
    Renderer r;

    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Renderer>();
    }

    public void StopParallax()
    {
        pause = true;
    }

    public void StartParallax()
    {
        pause = false;
    }

    public void BgDamageBlink()
    {
        StartCoroutine(ParallaxBlink(new Color(1,0,1)));
    }

    public void BgAttackBlink()
    {
        StartCoroutine(ParallaxBlink(Color.green));
    }

    IEnumerator ParallaxBlink(Color col)
    {
        r.material.color = col;
        yield return new WaitForSeconds(0.2f);
        r.material.color = Color.white;
    }


    // Update is called once per frame
    void Update()
    {
        if (pause) return;

        float tempOffset = offset * Time.time * speed;
        r.material.SetFloat("_scroll", tempOffset);
    }
}
