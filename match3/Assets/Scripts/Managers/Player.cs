﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public enum Stat
{
    hp,
    atk,
    matk,
    def
}

[System.Serializable]
public struct PlayerStat
{
    public Stat stat;
    public int value;
    public int price;
    public int level;
    public int amountPerLevel;
    public SpriteResEnum sprite;

    public void Add()
    {
        value += amountPerLevel;
        level++;
    }

    public int GetPrice()
    {
        return price * level;
    }
}

class Player : Singleton<Player>
{
    protected Player() { }

    public PlayerStats stats = new PlayerStats();

    public GameObject playerSpritePrefab;
    GameObject playerSprite;

    public Text textCurrentHp;
    public Image imageCurrentHP;

    public Text textStatsHp;
    public Text textStatsAtk;
    public Text textStatsMAtk;
    public Text textStatsDef;
    public Text textStatsGold;

    bool isDying = false;

    Animator animator;

    // Start is called before the first frame update
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
        BattleController.OnBattleWin += OnBattleWin;
        BattleController.OnBattleLose += OnBattleLose;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
        BattleController.OnBattleWin -= OnBattleWin;
        BattleController.OnBattleLose -= OnBattleLose;
    }

    void OnBattleWin()
    {
        Win();
        Wipe();
    }
    void OnBattleLose()
    {

    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void Initialize()
    {
        LoadPlayer(GameDataController.Instance.data.basicStats);
    }

    public void LoadPlayer(PlayerStats ps)
    {
        GameDataController.Instance.data.basicStats = ps;
        stats = ps;
        UpdateAllStatsUI();
    }


    public void ModifyMatk(int i)
    {

        stats.matk.value += i;
        StatsMAtkTextUpdate();
    }

    public void PreparePlayer()
    {
        stats = GetBasicStats();
        UpdateAllStatsUI();
    }

    void UpdateAllStatsUI()
    {
        if (BattleController.Instance.inRound)
        {
            UpdateUIwithCurrentStats();
        }

        else
        {
            UpdateUIwithBasicStats();
        }
    }

    public void UpdateUIwithCurrentStats()
    {
        PlayerStats ps = GameDataController.Instance.data.basicStats;

        textStatsGold.text = ps.gold.ToString();

        CurrentHpTextUpdate();
    }

    public void UpdateUIwithBasicStats()
    {
        PlayerStats temp = GameDataController.Instance.data.basicStats;
        textStatsGold.text = temp.gold.ToString();

        textStatsHp.text = temp.hp.value.ToString();
        textStatsAtk.text = temp.atk.value.ToString();
        textStatsMAtk.text = temp.matk.value.ToString();
        textStatsDef.text = temp.def.value.ToString();
    }

    public void GetDamage(int rawDamage)
    {
        stats.hp.value -= CalculateDamage(rawDamage);
        animator.SetTrigger("getdamage");
        Parallax.Instance.BgDamageBlink();
    }

    int CalculateDamage(int rawDamage)
    {
        int damage = rawDamage;

        if (stats.def.value > 0)
        {
            damage -= stats.def.value;
        }

        if (damage < 0)
        {
            damage = 0;
        }

        return damage;
    }

    public void Heal(int i)
    {
        PlayerStats temp = GameDataController.Instance.data.basicStats;

        if (stats.hp.value >= temp.hp.value) return; // если хп полные ничего не делаем TODO: потом добавить проверку на hp*level

        int healAmount = i;

        if (temp.hp.value - stats.hp.value < healAmount)
        {
            healAmount = temp.hp.value - stats.hp.value; // если хил превышает макс хп, уменьшаем его
        }


        stats.hp.value += healAmount;
        UpdateAllStatsUI();

        Parallax.Instance.BgAttackBlink();
    }

    public void MagicAttack()
    {
        EnemyController.Instance.DoDamage(GetMatchCount() * GetBasicStats().matk.value);
        MagicAttackAnimation();
    }

    public GameData GetGameData()
    {
        return GameDataController.Instance.data;
    }

    public BattleData GetBattleData()
    {
        return GameDataController.Instance.data.battleData;
    }
    public PlayerStats GetBasicStats()
    {
        return GameDataController.Instance.data.basicStats;
    }

    public int GetMatchCount()
    {
        return BattleController.Instance.selection.Count;
    }

    public void ModifyDef(int i)
    {
        stats.def.value += i;

        if (stats.def.value < 0)
        {
            stats.def.value = 0;
        }

        PreparePlayer();
        StatsDefTextUpdate();
    }

    public void StartRound()
    {
        PreparePlayer();
        animator.SetTrigger("approach");
        AudioController.Instance.PlayMoveSound();
    }

    public void Attack()
    {
        EnemyController.Instance.DoDamage(GetMatchCount() * GetBasicStats().atk.value);
        AttackAnimation();
    }

    void AttackAnimation()
    {
        Parallax.Instance.BgAttackBlink();
        animator.SetTrigger("attack");
    }

    void MagicAttackAnimation()
    {
        Parallax.Instance.BgAttackBlink();
        animator.SetTrigger("fireball");
    }

    public void SpawnPlayerObject()
    {
        if (playerSprite != null) return;

        Vector3 pos = new Vector3
        (
            transform.position.x,
            transform.position.y,
            10
        );

        playerSprite = Instantiate(playerSpritePrefab, pos, Quaternion.identity);
        playerSprite.transform.localScale = Vector3.one;
        playerSprite.transform.SetParent(transform);
    }

    void CurrentHpTextUpdate()
    {
        textCurrentHp.text = "HP: " + stats.hp.value.ToString() + "/" + GameDataController.Instance.data.basicStats.hp.value;
        float fill = (float)stats.hp.value / (float)GameDataController.Instance.data.basicStats.hp.value;

        imageCurrentHP.fillAmount = fill;

        if (fill < 0.5f)
        {
            imageCurrentHP.material.color = Color.red;
        }
        else imageCurrentHP.material.color = Color.green;

    }

    void StatsAtkTextUpdate()
    {
        textStatsAtk.text = stats.atk.value.ToString();
    }

    void StatsHpTextUpdate()
    {
        textStatsHp.text = stats.hp.value.ToString();
    }

    void StatsDefTextUpdate()
    {
        textStatsDef.text = stats.def.value.ToString();
    }

    void StatsGoldTextUpdate()
    {
        textStatsGold.text = stats.gold.ToString();
    }

    void StatsMAtkTextUpdate()
    {
        textStatsMAtk.text = stats.matk.value.ToString();
    }

    public void Win()
    {
        animator.SetTrigger("win");
        AudioController.Instance.PlayVictorySound();
        PreparePlayer();
    }

    public void Lose()
    {
        animator.SetTrigger("lose");
        AudioController.Instance.PlayDeathSound();
        PreparePlayer();
    }

    public void Update()
    {
        if (!BattleController.Instance.inRound || !BattleController.Instance.canControl) return;

        if (stats.hp.value <= 0 && !isDying)
        {
            StartCoroutine(Die());
        }
    }

    private void FixedUpdate()
    {
        UpdateAllStatsUI();
    }

    public void Wipe()
    {
        PreparePlayer();
    }


    IEnumerator Die()
    {
        isDying = true;
        yield return new WaitForSeconds(1);
        BattleController.Instance.CancelRound();
        stats = GameDataController.Instance.data.basicStats;
        isDying = false;
    }


    public void BuyStat(Stat stat)
    {
        if (!CanAfford(stat)) return;

        GameDataController.Instance.ChangeBalance( -GetBasicStat(stat).price * GetBasicStat(stat).level);

        AddStat(stat);

        ShopController.Instance.Initialize();

        UpdateAllStatsUI();
        //GameDataController.Instance.SaveGameDataToFirebase();
        //GameDataController.Instance.Save();
    }

    void AddStat(Stat stat)
    {
        int amount = GetBasicStat(stat).value + GetBasicStat(stat).amountPerLevel;

        switch (stat)
        {
            case (Stat.hp):
                {
                    GameDataController.Instance.data.basicStats.hp.value = amount;
                    GameDataController.Instance.data.basicStats.hp.level++;
                    break;
                }
            case (Stat.atk):
                {
                    GameDataController.Instance.data.basicStats.atk.value = amount;
                    GameDataController.Instance.data.basicStats.atk.level++;
                    break;
                }
            case (Stat.matk):
                {
                    GameDataController.Instance.data.basicStats.matk.value = amount;
                    GameDataController.Instance.data.basicStats.matk.level++;
                    break;
                }
            case (Stat.def):
                {
                    GameDataController.Instance.data.basicStats.def.value = amount;
                    GameDataController.Instance.data.basicStats.def.level++;
                    break;
                }
        }
    }

    PlayerStat GetBasicStat(Stat stat)
    {
        PlayerStats basicStats = GameDataController.Instance.data.basicStats;
        PlayerStat s = new PlayerStat();

        switch (stat)
        {
            case (Stat.hp):
                {
                    s = basicStats.hp;
                    break;
                }
            case (Stat.atk):
                {
                    s = basicStats.atk;
                    break;
                }
            case (Stat.matk):
                {
                    s = basicStats.matk;
                    break;
                }
            case (Stat.def):
                {
                    s = basicStats.def;
                    break;
                }
        }

        return s;
    }

    bool CanAfford(Stat stat)
    {
        if (GetBasicStat(stat).level * GetBasicStat(stat).price <= GetBasicStats().gold)
        {
            return true;
        }
        else return false;
    }
}
