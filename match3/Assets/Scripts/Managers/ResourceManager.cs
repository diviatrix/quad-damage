﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceManager : Singleton<ResourceManager>
{
    public SpriteResourceDataSO sprites;


    public Sprite GetSprite(SpriteResEnum resEnum)
    {
        Sprite temp = null;

        foreach (SpriteResourceData s in sprites.resources)
        {
            if (s.name == resEnum)
            {
                temp = s.sprite;
            }
        }

        return temp;
    }


}
