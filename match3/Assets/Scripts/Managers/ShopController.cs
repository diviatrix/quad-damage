﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ShopController : Singleton<ShopController>
{
    [Header("HP objects")]
    public Text hpText;
    public Image hpImage;
    public Sprite hpSprite;

    [Header("Atk objects")]
    public Text atkText;
    public Image atkImage;
    public Sprite atkSprite;

    [Header("MAtk objects")]
    public Text matkText;
    public Image matkImage;
    public Sprite matkSprite;

    [Header("Def objects")]
    public Text defText;
    public Image defImage;
    public Sprite defSprite;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }

    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void Initialize()
    {
        SetButtons();
    }

    void SetButtons()
    {
        PlayerStats stats = GameDataController.Instance.data.basicStats;

        hpImage.sprite = ResourceManager.Instance.GetSprite(SpriteResEnum.heart);
        hpText.text = (
                    "Buy " +
                    stats.hp.amountPerLevel + " " +
                    stats.hp.stat + " for " +
                    (stats.hp.GetPrice()) +
                    " gold");

        atkImage.sprite = ResourceManager.Instance.GetSprite(SpriteResEnum.Atk);
        atkText.text = (
                    "Buy " +
                    stats.atk.amountPerLevel + " " +
                    stats.atk.stat + " for " +
                    (stats.atk.GetPrice()) +
                    " gold");

        matkImage.sprite = ResourceManager.Instance.GetSprite(SpriteResEnum.mAtk);
        matkText.text = (
                    "Buy " +
                    stats.matk.amountPerLevel + " " +
                    stats.matk.stat + " for " +
                    (stats.matk.GetPrice()) +
                    " gold");

        defImage.sprite = ResourceManager.Instance.GetSprite(SpriteResEnum.shield);
        defText.text = (
                    "Buy " +
                    stats.def.amountPerLevel + " " +
                    stats.def.stat + " for " +
                    (stats.def.GetPrice()) +
                    " gold");

    }
}
