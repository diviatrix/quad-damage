﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileManager: Singleton<TileManager>
{
    public TileData[] tileDatas;

    public GameObject MakeTileGO(int x, int y)
    {
        // create scene tile block
        GameObject tileGO = new GameObject();
        tileGO.AddComponent<CircleCollider2D>();
        tileGO.AddComponent<SpriteRenderer>();
        tileGO.name = x + "," + y;
        tileGO.transform.position = new Vector2(x, y);
        tileGO.layer = 1;

        TileLogic tileLogic = tileGO.AddComponent<TileLogic>();
        tileLogic.tileDatas = tileDatas;
        //tileLogic.Activate();

        return tileGO;
    }

    public TileData GetRandomedTileData()
    {
        int r = Random.Range(0, tileDatas.Length);
        return tileDatas[r];
    }

    public TileData SearchTileDataByEnum(TileEnum te)
    {
        TileData outData = null;

        foreach (TileData td in tileDatas)
        {
            if (te == td.tileEnum)
            {
                outData = td;
            }
        }

        return outData;
    }

    public void UpdateTileAtCoord(int x, int y)
    {
        Board.Instance.GOfield[x, y].GetComponent<TileLogic>().currentTileData = Board.Instance.TileField[x, y].currentTileData;
        Board.Instance.GOfield[x, y].GetComponent<TileLogic>().Activate();
    }
}
