﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EnemyInLevel
{
    public GameObject EnemyPrefab;
    public int amount;
}

[System.Serializable]
public struct LevelSettings
{
    public int id;
    public string text;
    public bool open;
    public bool repeatable;
}


[System.Serializable]
[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order = 1)]
public class LevelSO : ScriptableObject
{
    [Header("Bools")]
    public LevelSettings Level_Settings;

    [Header("Enemy List")]
    public List<EnemyInLevel> enemies;    
}
