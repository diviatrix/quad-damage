﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTrigger : MonoBehaviour
{
    public bool buyStat = true;
    public Stat stat;

    public bool addGold;
    public int gold;

    public void Buy()
    {
        if (addGold)
        {
            PanelController.Instance.PlayCoinAnimation(gold);
        }

        if (buyStat) { Player.Instance.BuyStat(stat); }

        GameDataController.Instance.ReuqestSave();
    }

}
