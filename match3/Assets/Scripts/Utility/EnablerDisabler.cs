﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnablerDisabler : MonoBehaviour
{
    public List<GameObject> toEnable;
    public List<GameObject> toDisable;

    public bool isSwitcher;
    public GameObject switchGO;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject go in toEnable)
        {
            go.SetActive(true);
        }
        foreach (GameObject go in toDisable)
        {
            go.SetActive(false);
        }
    }

    public void Switch()
    {
        switchGO.SetActive(!switchGO.activeSelf);
    }
}
