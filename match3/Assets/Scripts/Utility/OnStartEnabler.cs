﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartEnabler : MonoBehaviour
{
    public bool EnableOnStartRound;
    private void OnCreated()
    {
        gameObject.SetActive(EnableOnStartRound);
    }
}
