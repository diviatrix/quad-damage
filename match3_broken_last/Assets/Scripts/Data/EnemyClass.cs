﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyClass : MonoBehaviour
{
    public int id;

    public bool isBounty;

    [Header("Prefabs")]
    public GameObject DeathPrefab;
    public Sprite levelInfoSprite;

    [Header("Default Battle Stats")]
    public int maxHp;    
    
    public int attack_damage;    
    public float attack_delay;

    public string monsterName;

    [Header("Game Stats")]
    public int price;

    [Header("Current Battle Stats")]
    public int hp;

    [Header("Extra skills")]
    public List<SimpleEvent> skills;

    // privates
    Animator animator;

    [HideInInspector]
    public float lastAttackTime;
    bool dying = false;

    void Start()
    {
        UpdateLastAttackTime();
        animator = GetComponent<Animator>();
    }

    public void UpdateLastAttackTime()
    {
        lastAttackTime = Time.time;
    }

    public void SetHp()
    {
        hp = maxHp;
    }
    void modifyhp(int i)
    {
        hp += i;
    }

    public void GetDamage(int i)
    {
        animator.SetTrigger("getdamage");
        modifyhp(-i);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!BattleController.Instance.canControl) return;
        if (dying) return;

        if (hp <= 0)
        {
            StartCoroutine(Die());
        }

        if (lastAttackTime + attack_delay <= Time.time)
        {
            Attack();
        }

    }

    void Attack()
    {
        animator.SetTrigger("attack");
        AudioController.Instance.PlayAtkSound();
        UpdateLastAttackTime();
        Player.Instance.GetDamage(attack_damage);
    }

    IEnumerator Die()
    {        
        dying = true;
        Parallax.Instance.StartParallax();

        CollectionManager.Instance.Open(EnemyController.Instance.enemyQueue[0]);
        CollectionManager.Instance.Initialize();

        Instantiate(DeathPrefab, transform.position, transform.rotation);

        IngameLevelsData ld = GameDataController.Instance.data.ingameLevelsData;
        

        if (isBounty && ld.levels[ld.currentLevel].enableBounty)
        {
            
            PanelController.Instance.PlayCoinAnimation(price);

            if (!ld.levels[ld.currentLevel].repeatable)
            {
                GameDataController.Instance.data.ingameLevelsData.levels[ld.currentLevel].enableBounty = false;
            }
        }

        else if (!isBounty)
        {
            GameDataController.Instance.data.basicStats.gold += price;
            PanelController.Instance.PlayCoinAnimation(price);
        }
        

        yield return new WaitForSeconds(1);

        EnemyController.Instance.enemyQueue.RemoveAt(0);
        EnemyController.Instance.enemyObject = null;

                
        Destroy(gameObject);          
    }      
}
