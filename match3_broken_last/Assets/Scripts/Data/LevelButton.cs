﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [Header("Setup")]
    public int id;
    public LevelSO levelSO;

    [Header("Objects")]
    public Image levelIcon;
    public Text levelName;

    /*
    Бля, тут просто надо сделать чтобы эти сраные кнопки добавляли инфу с себя в геймдату после загрузки, если этих данных нет (вообще). 
    тут метод пулла в геймдату (при получении эвента что геймдата загружена) 
    в геймдате метод который смотрит, если такой ID есть, то нахуй, если нет, добавляем. типа того чота.
    пиздец оно меня заебало, еще кучу кода упростить надо будет.

    или все таки доделать то что сейчас. хуууй знает. не, сейчас слишком много говнокод для проверок. 

    но сначала проспаться.




    __


    нихуя, 

    в самом геймдата контроллере, при загрузке, проверяем, есть ли левелдата, если нету, ставим дефолтную. 
    А дефолтную создаем из кнопок опрашивая их как-нибудь.

    */

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
        GetComponent<Button>().onClick.AddListener(ShowLevelPanel);
    }

    public void ShowLevelPanel()
    {
        PanelController.Instance.OpenLevelInfoPopup();

        LevelInfoScript.Instance.UpdateData(this);        
    }

    public void Initialize()
    {
        bool hasIngameData = false;
        bool isOpen = false;

        // GameDataController.Instance.data.ingameLevelsData.GetLevel(id);

        if (LevelManager.Instance.LevelExistInData(id))
        {
            hasIngameData = true;
        }

        
        if(hasIngameData)
        {
            isOpen = GameDataController.Instance.data.ingameLevelsData.GetLevel(id).open;
        }
        else
        {
            isOpen = levelSO.open;            
        }

        levelIcon.gameObject.SetActive(isOpen);
        levelName.gameObject.SetActive(isOpen);

        GetComponent<Button>().interactable =  isOpen;

        levelIcon.sprite = levelSO.sprite;
        levelName.text = levelSO.text;

        
    }

    void SetSoDefaults()
    {
        
    }
}
