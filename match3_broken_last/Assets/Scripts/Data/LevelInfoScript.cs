﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInfoScript : Singleton<LevelInfoScript>
{
    [Header("Basic Settings")]
    public Text soText;
    public int level_id;

    [Header("Monster links")]
    public Image basicMonsterImage;

    [Header("Bounty links")]
    public Text bountyText;


    [Header("Debug")]
    public List<Image> monsterImages;

    [Header("Privates")]
    private LevelSO currentSO;
    private int currentID;


    public void UpdateData(LevelButton levelButton)
    {
        level_id = levelButton.id;

        SetMonsters(levelButton);
        SetBounty(levelButton);

        soText.text = levelButton.levelSO.text;   
    }

    public void StartGame()
    {
        BattleController.Instance.StartGameWithLevel(currentID, currentSO);
    }

    void DestroyMonsterImages()
    {
        foreach (Image image in monsterImages) Destroy(image.gameObject);
        monsterImages = new List<Image>();
    }

    void SetMonsters(LevelButton levelButton)
    {
        DestroyMonsterImages();
        basicMonsterImage.gameObject.SetActive(false);

        //Rect basicRect = basicMonsterImage.rectTransform.rect;
        int i = 0;
        foreach (EnemyInLevel enemy in levelButton.levelSO.enemies)
        {
            Image temp = Instantiate(basicMonsterImage, basicMonsterImage.transform.parent);
            temp.gameObject.SetActive(true);
            temp.sprite = enemy.EnemyPrefab.GetComponent<EnemyClass>().levelInfoSprite;
            temp.GetComponentInChildren<Text>().text = "x " +  enemy.amount.ToString();
            monsterImages.Add(temp);            
        }

        foreach (Image img in monsterImages)
        {
            img.transform.localPosition = new Vector2(img.transform.localPosition.x + i++ * img.GetComponent<RectTransform>().rect.width*2 ,img.transform.localPosition.y);
        }
    }

    void SetBounty(LevelButton levelButton)
    {
        List<EnemyInLevel> lvl = levelButton.levelSO.enemies;
        currentSO = levelButton.levelSO;
        currentID = levelButton.id;

        int bonus = 0;

        

        if (GameDataController.Instance.data.ingameLevelsData.levels.Count > level_id)
        {
            if (GameDataController.Instance.data.ingameLevelsData.GetLevel(level_id).enableBounty)
            {
                foreach (EnemyInLevel enemy in lvl)
                {
                    if (enemy.EnemyPrefab.GetComponent<EnemyClass>().isBounty)
                    {
                        bonus += enemy.amount * enemy.EnemyPrefab.GetComponent<EnemyClass>().price;
                    }
                }

                bountyText.text = "Bounty of <b><color=orange>" + bonus + "</color></b> gold is enabled for this round";
            }
            else
            {
                foreach (EnemyInLevel enemy in lvl)
                {
                    if (!enemy.EnemyPrefab.GetComponent<EnemyClass>().isBounty)
                    {
                        bonus += enemy.amount * enemy.EnemyPrefab.GetComponent<EnemyClass>().price;
                    }
                }

                bountyText.text = "No bounty for this round, you will get <b><color=orange>" + bonus + "</color></b> gold from monsters";
            }
        }                
    }
    
}
