﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerInfoScript : MonoBehaviour
{
    public Text email;
    public Text password;
    public Text name;
    public Image profileImage;

    void Awake()
    {
        FirebaseManager.OnAuth += OnAuth;
    }
    void OnDestroy()
    {
        FirebaseManager.OnAuth -= OnAuth;
    }

    void OnAuth()
    {
        Debug.Log("Event: OnAuth called");

        email.text = FirebaseManager.Instance.user.Email.ToString();
        name.text = FirebaseManager.Instance.user.DisplayName;

        DonwloadImage();
    }

    IEnumerator DonwloadImage()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(FirebaseManager.Instance.user.PhotoUrl);
        yield return www.SendWebRequest();
        Texture2D myTexture = DownloadHandlerTexture.GetContent(www);

        Sprite sprite = Sprite.Create(myTexture, profileImage.rectTransform.rect, profileImage.rectTransform.pivot);
        profileImage.sprite = sprite;
    }
}
