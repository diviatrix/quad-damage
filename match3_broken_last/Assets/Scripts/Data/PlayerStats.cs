﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PlayerStats
{
    [Header("Game Stats")]
    public int gold;

    [Header("Player Stats")]
    public PlayerStat hp;
    public PlayerStat atk;
    public PlayerStat matk;
    public PlayerStat def;

    void Awake()
    {
        hp.stat = Stat.hp;
        atk.stat = Stat.atk;
        matk.stat = Stat.matk;
        def.stat = Stat.def;
    }
}