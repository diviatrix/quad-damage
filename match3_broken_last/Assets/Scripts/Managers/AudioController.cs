﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AudioData
{
    public float musicVolume;
    public bool musicEnabled;

    public float audioVolume;
    public bool audioEnabled;
}

public class AudioController : Singleton<AudioController>
{
    [Header("Players")]
    public AudioSource audioPlayer;
    public AudioSource musicPlayer;

    [Header("Toggles")]
    public Toggle musicToggle;
    public Slider musicSlider;

    public Toggle audioToggle;
    public Slider audioSlider;

    [Header("Clips")]
    public AudioClip popSound;
    public AudioClip slideSound;
    public AudioClip click;
    public AudioClip death;
    public AudioClip victory;
    public AudioClip heal;
    public AudioClip attack;
    public AudioClip magicattack;
    public AudioClip move;
    public AudioClip coin;

    [Header("Music")]
    public AudioClip musicSound;
    //public bool isMusicPlaying;

    public AudioData audiodata;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    // Start is called before the first frame update
    void Awake()
    {  
        audioPlayer = gameObject.AddComponent<AudioSource>();

        musicPlayer = gameObject.AddComponent<AudioSource>();
        musicPlayer.clip = musicSound;
        musicPlayer.loop = true;
    }    

    public void Initialize()
    {
        audiodata = GameDataController.Instance.data.audioData;
        
        UpdateUI();

        SetAudioVolume();
        SetMusicVolume();

        if(audiodata.musicEnabled) musicPlayer.Play();


    }

    public void SetMusicVolume()
    {
        musicPlayer.volume = musicSlider.value;
        audiodata.musicVolume = musicSlider.value;
    }

    public void SetAudioVolume()
    {
        audioPlayer.volume = audioSlider.value;
        audiodata.audioVolume = audioSlider.value;
    }


    public void UpdateUI()
    {    
        musicToggle.isOn = audiodata.musicEnabled;
        musicSlider.value = audiodata.musicVolume;

        audioToggle.isOn = audiodata.audioEnabled;
        audioSlider.value = audiodata.audioVolume;
    }


    public void SwitchMusic()
    {
        if (musicToggle.isOn)
        {
            audiodata.musicEnabled = true;
            musicPlayer.Play();
        }
        else
        {
            audiodata.musicEnabled = false;
            musicPlayer.Stop();
        }
    }


    public void SwitchAudio()
    {
        if (audioToggle.isOn)
        {
            audiodata.audioEnabled = true;
            audioPlayer.Play();
        }
        else
        {
            audiodata.audioEnabled = false;
            audioPlayer.Stop();
        }
    }


    public void PlayPopSound()
    {
        audioPlayer.PlayOneShot(popSound);
    }

    public void PlayCoinSound()
    {
        audioPlayer.PlayOneShot(coin);
    }
    public void PlaySlideSound()
    {
        audioPlayer.PlayOneShot(slideSound);
    }
    public void PlayClickSound()
    {
        audioPlayer.PlayOneShot(click);
    }
    public void PlayDeathSound()
    {
        audioPlayer.PlayOneShot(death);
    }
    public void PlayVictorySound()
    {
        audioPlayer.PlayOneShot(victory);
    }
    public void PlayHealSound()
    {
        audioPlayer.PlayOneShot(heal);
    }
    public void PlayAtkSound()
    {
        audioPlayer.PlayOneShot(attack);
    }
    public void PlayMAtkSound()
    {
        audioPlayer.PlayOneShot(magicattack);
    }
    public void PlayMoveSound()
    {
        audioPlayer.PlayOneShot(move);
    }

    public void PlaySound(AudioClip clip)
    {
        audioPlayer.PlayOneShot(clip);
    }


    
}
