﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class SimpleEvent : UnityEvent { };

public class Board : Singleton<Board>
{
    public GameObject SelectionPrefab;
    public GameObject ExplosionPrefab;
    public int width;
    public int height;

    public Sprite bgSprite1, bgSprite2;
    GameObject bgField;
    
    public Tile[,] TileField; // поле тайлов в коде, с ним все операции
    public GameObject[,] GOfield; // поле тайлов на сцене, только визуализация и клики
    //public TileData[] tilePrefabs;
    // Start is called before the first frame update
    void Start()
    {          
        Setup();
        //UpdateSprites();        
    }

    public void ExplodeTile(GameObject go)
    {
        int i = (int)go.transform.position.x;
        int j = (int)go.transform.position.y;
        TileField[i, j].Clear();
        UpdateSprites();
        go.GetComponent<TileLogic>().Explode();
    }

    public void FillTopRow()
    {
        for (int x = 0; x < width; x++)
        {
            if (TileField[x, height - 1].IsEmpty())
            {
                TileField[x, height - 1] = new Tile();
                
            }
            UpdateSprites();
        }
    }  
    
    public void FillField()
    {
        foreach (Tile tile in TileField)
        {
            tile.Randomize();
            UpdateSprites();
        }
        UpdateSprites();
    }

    public void WipeField()
    {

        foreach (Tile tile in TileField)
        {
            tile.Clear();
        }
        UpdateSprites();
    }

    public void SetupBgField()
    {
        bgField = new GameObject("BgField");

        bool setFistPrefab = true;

        foreach (GameObject go in GOfield)
        {
            if (setFistPrefab)
            {
                MakeBgTile(go, bgSprite1);
                setFistPrefab = false;
            }
            else if(!setFistPrefab)
            {
                MakeBgTile(go, bgSprite2);
                setFistPrefab = true;
            }
        }
    }

    GameObject MakeBgTile(GameObject go, Sprite sprite)
    {
        GameObject bgTile = new GameObject();
        bgTile.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, 1);
        bgTile.name = go.name;
        bgTile.transform.SetParent(bgField.transform);
        bgTile.AddComponent<SpriteRenderer>().sprite = sprite;
        return bgTile;
    }

    public void Setup()
    {     
        TileField = new Tile[width,height];
        
        GOfield = new GameObject[width,height];

        for(int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                TileField[i, j] = new Tile();
                GOfield[i, j] = TileManager.Instance.MakeTileGO(i, j);
                GOfield[i, j].transform.SetParent(transform);
            }
        }

        SetupBgField();
    } 

    public void UpdateSprites() // матчим матрицу с тайлами и реальные спрайты
    {
        for(int x = 0; x < width; x++)
        {
            for (int j = 0; j < height; j++)
            {
                TileManager.Instance.UpdateTileAtCoord(x, j);
            }
        }
    }   

    void RemoveEmptyTilesAtCol(int col) // говно 100% тут, прям блядь полюбас
    {
        List<Tile> rowTiles = new List<Tile>();

        for(int row = 0; row < height; row++)
        {
            if (!TileField[col, row].IsEmpty())
            {
                rowTiles.Add(new Tile(TileField[col, row].currentTileData));
                UpdateSprites();
            }
            UpdateSprites();
        }
        UpdateSprites();
        for (int row = 0; row < height; row++)
        {
            if (row < rowTiles.Count)
            {
                TileField[col, row] = new Tile(rowTiles[row].currentTileData);
                UpdateSprites();
            }
            else
            {
                TileField[col, row].Clear();
                UpdateSprites();
            }
        }
        UpdateSprites();
    }

    public void ElevateAllEmptyTiles()
    {   
        for(int x = 0; x < width; x++)     
        {
            RemoveEmptyTilesAtCol(x);
            UpdateSprites();
        }
    }

    public bool HasEmptyTiles()
    {
        foreach(Tile tile in TileField)
        {
            if(tile.IsEmpty())
            {
                return true;
            }             
        }        
        return false;
    }    
}
