﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CollectionData
{
    public List<GameObject> cards;
}

[System.Serializable]
public class CollectionManager : Singleton<CollectionManager>
{
    public GameObject basicCard;
    public Transform content;

    public List<GameObject> buttons;
    // Start is called before the first frame update
    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void Open(GameObject card)
    {
        if (GameDataController.Instance.data.collectionData.cards != null)
        {
            foreach (GameObject go in GameDataController.Instance.data.collectionData.cards)
            {
                if (card == go)
                {
                    return;
                }
            }
        }
        else
        {
            GameDataController.Instance.data.collectionData.cards = new List<GameObject>();
        }

        GameDataController.Instance.data.collectionData.cards.Add(card);
    }

    public void Initialize()
    {
        WipeButtons();

        int i = 0;
        basicCard.SetActive(false);

        if (GameDataController.Instance.data.collectionData.cards == null) return;

        foreach (GameObject go in GameDataController.Instance.data.collectionData.cards)
        {
            GameObject temp = Instantiate(basicCard, content);
            content.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(content.gameObject.GetComponent<RectTransform>().sizeDelta.x, basicCard.GetComponent<RectTransform>().sizeDelta.y * i);
            temp.SetActive(true);
            //temp.transform.Translate(Vector3.down * 75 * i++, content);
            temp.GetComponent<RectTransform>().localPosition = new Vector2(basicCard.transform.localPosition.x, basicCard.transform.localPosition.y -  basicCard.GetComponent<RectTransform>().sizeDelta.y * i);
            temp.GetComponent<CardData>().monsterImage.sprite = go.GetComponent<EnemyClass>().levelInfoSprite;
            temp.GetComponent<CardData>().monsterHP.text = "Monster HP: " + go.GetComponent<EnemyClass>().maxHp.ToString();
            temp.GetComponent<CardData>().monsterDamage.text = "Monster Attack: " + go.GetComponent<EnemyClass>().attack_damage.ToString();
            temp.GetComponent<CardData>().monsterGold.text = "Gold Amount: " + go.GetComponent<EnemyClass>().price.ToString();
            temp.GetComponent<CardData>().monsterAtkRate.text = "Monster Attack rate: " + go.GetComponent<EnemyClass>().attack_delay.ToString() + " s.";
            temp.GetComponent<CardData>().monsterName.text = "Monster name: <color=brown>" + go.GetComponent<EnemyClass>().monsterName + "</color>";

            buttons.Add(temp);
            i++;
        }
    }

    void WipeButtons()
    {
        if (buttons != null)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                Destroy(buttons[i]);
            }
        }        

        buttons = new List<GameObject>();
    }
}
