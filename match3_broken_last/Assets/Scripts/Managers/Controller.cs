﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class Controller : Singleton<Controller>
{
    [Header("Controllers")]
    public List<GameObject> Controllers;

    //public SimpleEvent PlaySlideSound = new SimpleEvent();

    void Start()
    {
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void Initialize()
    {

    }        
}
