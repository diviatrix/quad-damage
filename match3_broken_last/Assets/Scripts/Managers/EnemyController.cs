﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : Singleton<EnemyController>
{
    public List<GameObject> enemyQueue = null;

    public Text enemyHpText;
    public Image enemyImage;
    public Image enemyAttackTimerImage;

    public GameObject enemyObject;

    public float respawnCheckTime = 1;
    public float lastRespawnCheckTime;

    private void Update()
    {
        UpdateAttackTimer();
    }
    public void SpawnEnemyObject()
    {       

        if (enemyObject != null) return;

        Vector3 pos = new Vector3
        (
            transform.position.x,
            transform.position.y,
            10
        );
        enemyObject = Instantiate(enemyQueue[0], pos, Quaternion.identity);
        enemyObject.transform.SetParent(transform);
        enemyObject.GetComponent<Animator>().SetTrigger("approach");
        enemyObject.GetComponent<EnemyClass>().SetHp();

        //enemyQueue.Remove(enemyQueue[0]);
        EnemyHPUpdate();
    }


    public void FixedUpdate()
    {
        if (BattleController.Instance.inRound) CheckEnemiesLeft();        
    }
    public void EnemyHPUpdate()
    {
        EnemyClass currentEnemy;

        // update text
        if (enemyObject != null)
        {
            currentEnemy = enemyObject.GetComponent<EnemyClass>();

            enemyHpText.text = "HP: " + currentEnemy.hp.ToString() + "/" + currentEnemy.maxHp.ToString();

            float fill = (float)currentEnemy.hp / (float)currentEnemy.maxHp;
           
            enemyImage.fillAmount = fill;

            if (fill < 0.25f)
            {
                enemyImage.material.color = Color.red;
            }
            else enemyImage.material.color = Color.green;
        }
        else
        {
            enemyHpText.text = "";
        }
    }

    void UpdateAttackTimer()
    {
        EnemyClass currentEnemy;

        // update text
        if (enemyObject != null)
        {
            currentEnemy = enemyObject.GetComponent<EnemyClass>();

            float timeTillAttack = Time.time - currentEnemy.lastAttackTime;

            float fill = timeTillAttack / currentEnemy.attack_delay;
            //float fill = Time.time / atkTime;
            enemyAttackTimerImage.fillAmount = fill;
        }
        else
        {
            enemyAttackTimerImage.fillAmount = 0;
        }
    }

    public void CheckEnemiesLeft()
    {              
        if (lastRespawnCheckTime + respawnCheckTime >= Time.time) return;
        lastRespawnCheckTime = Time.time;

        if (!BattleController.Instance.canControl) return;
        if (enemyObject != null) return;


        if (enemyQueue.Count <= 0 && enemyObject == null)
        {
            BattleController.Instance.WinRound();
        }
        else if (enemyQueue.Count > 0 && enemyObject == null)
        {
            SpawnEnemyObject();
        }
    }

    // Start is called before the first frame update

    public void StartRound(List<EnemyInLevel> enemeiesInLevel)
    {
        Wipe();
        foreach (EnemyInLevel e in enemeiesInLevel)
        {
            int count;

            for (count = e.amount; count > 0; count--) enemyQueue.Add(e.EnemyPrefab);
        }

        lastRespawnCheckTime = Time.time;      
    }


    public void DoDamage(int i)
    {
        if (enemyObject == null)
        {
            Debug.LogWarning("Tried to damage enemy, but it does not exist");
            return;
                
        }
        enemyObject.GetComponent<EnemyClass>().GetDamage(i);
        //CheckEnemiesLeft();
    }

    public void Wipe()
    {
        enemyQueue = new List<GameObject>();
        Destroy(enemyObject);
        EnemyHPUpdate();
    }
}
