﻿using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;


public class FirebaseManager : Singleton<FirebaseManager>
{
    public delegate void FirebaseDelegate();
    public static event FirebaseDelegate OnAuth;


    [Header("Input Fields")]
    public InputField emailField;
    public InputField pwdField;
    public Text status;

    [Header("Firebase data")]
    public FirebaseUser user;
    public FirebaseUser appUser;
    Firebase.Auth.FirebaseAuth auth;

    [Header("Debug")]
    public string email;
    public string password;

    void Awake()
    {
        StartCoroutine(InitializeFirebase());
    }

    IEnumerator InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);

        yield return new WaitForSeconds(0);
    }
 
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                SetStatusText("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;

            if (signedIn)
            {
                SetStatusText("Signed in " + user.Email);            
            }
        }
    }

    void SetStatusText(String text)
    {
        status.text = text;
    }

    public void Register()
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                SetStatusText("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                SetStatusText("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            user = newUser;
            SetStatusText( "User created successfully:" + newUser.DisplayName + ", " + newUser.UserId);
        });
    }

    public void Login()
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => 
        {
            if (task.IsCanceled)
            {
                SetStatusText("SignInWithEmailAndPasswordAsync was canceled.");
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                SetStatusText("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            
            user = newUser;

            OnAuth();

        });        
    }
    /*
    // Start is called before the first frame update
    public void FacebookLogin()
    {
        Firebase.Auth.Credential credential = Firebase.Auth.FacebookAuthProvider.GetCredential(accessToken);

        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
        if (task.IsCanceled) {
            Debug.LogError("SignInWithCredentialAsync was canceled.");
            return;
        }
        if (task.IsFaulted) {
            Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
            return;
        }

        Firebase.Auth.FirebaseUser newUser = task.Result;
        Debug.LogFormat("User signed in successfully: {0} ({1})",
            newUser.DisplayName, newUser.UserId);
        });
    }
    */

    public void SetPassword()
    {
        password = pwdField.text;
    }
    public void SetEmail()
    {
        email = emailField.text;
    }
}
