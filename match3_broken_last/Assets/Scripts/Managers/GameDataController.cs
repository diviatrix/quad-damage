﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataController : Singleton<GameDataController>
{
    public delegate void GameDataDelegate();
    public static event GameDataDelegate OnLoadGameData;

    public bool autoload;
    public GameVarsSO defaultData;
    public GameData data;

    private void Start()
    {
        if (autoload)
        {
            if (Load())
            {
                Debug.Log("Load gameData from disc successful");
            }
            else
            {
                SetDefaultData();                
            }
            OnLoadGameData();
            return;
        }
        else
        {
            SetDefaultData();
            OnLoadGameData(); 
        }

        
    }

    public void Save()
    {
        data.audioData = AudioController.Instance.audiodata;
        SaveSystem.Instance.SaveGame(data);
    }

    bool Load()
    {
        bool success = false;

        SaveData saveData = SaveSystem.Instance.LoadGameFromDisk();

        if (!saveData.isEmpty)
        {
            GameData temp = GameData.CreateFromJSON(saveData.gameData);
            if (temp.version == defaultData.gameData.version)
            {
                data = GameData.CreateFromJSON(saveData.gameData);
                success = true;
            }                
        }
        return success;        
    }

    void SetDefaultData ()
    {
        data = defaultData.gameData;
    }
}
