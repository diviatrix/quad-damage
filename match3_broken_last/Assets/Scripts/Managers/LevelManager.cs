﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

[System.Serializable]
public class IngameLevel
{
    public int id;
    public LevelSO so;
    public bool open;
    public bool repeatable;
    public bool enableBounty;

    public void SetOpen(bool b)
    {
        open = b;
    }

    public void SetDefaultsFromSO()
    {
        open = so.open;
        repeatable = so.repeatable;
        enableBounty = so.enableBounty;
    }
}

[System.Serializable]
public class IngameLevelsData
{
    [Header("Ingame level Stats")]
    public List<IngameLevel> levels;
    

    [Header("Level Stats")]
    public int lastLevel;
    public int currentLevel;

    public IngameLevel GetLevel(int id)
    {
        if (levels.Count > id) return levels[id];
        else return null;
    }

    public bool isLevelOpen(int id)
    {
        return GetLevel(id).open;
    }
    public void OpenLevel(int i, bool b)
    {
        if (i <= levels.Count)
        {
            levels[i].SetOpen(b);
        }        
    }

    public LevelSO GetCurrentLevelSO()
    {
        return levels[currentLevel +1].so;
    }

    public void UpdateLastLevel()
    {
        lastLevel = currentLevel;
    }
}

public class LevelManager : Singleton<LevelManager>
{
    [Header("Buttons")]
    public Transform buttonContainer;
    public IngameLevelsData data;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }
    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }


    public void Initialize()
    {
        data = GameDataController.Instance.data.ingameLevelsData;
    }

    public bool LevelExistInData(int id)
    {
        if (data.levels == null) return false;
        else if (data.levels.Count <= id) return false;
        else return true;
    }

    public void StartNextLevel()
    {
        if (data.currentLevel + 1 < data.levels.Count)
        {
            GameDataController.Instance.data.ingameLevelsData.currentLevel++;

            PanelController.Instance.GoToStartGame();

            BattleController.Instance.StartNewGameWithParams(data.GetCurrentLevelSO());
        }
        else PanelController.Instance.GoToMap();
    }

    public void StartSameLevel()
    {
        PanelController.Instance.GoToStartGame();
        BattleController.Instance.StartGameWithLevel(data.currentLevel, data.GetLevel(data.currentLevel).so);
    }

    public void WinRound()
    {
        OpenNextLevel();
        UpdateLastLevel();        
    }

    public void UpdateLastLevel()
    {
        GameDataController.Instance.data.ingameLevelsData.UpdateLastLevel();
    }

    public void OpenNextLevel()
    {
        data.OpenLevel(data.currentLevel+1, true);    
    }
          
    public void FillButtons()
    {
        foreach (Transform child in buttonContainer)
        {
            if (child.GetComponent<LevelButton>())
            {
                child.GetComponent<LevelButton>().Initialize();
            }
        }
    }
}
