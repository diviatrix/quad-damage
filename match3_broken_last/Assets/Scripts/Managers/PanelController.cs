﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PageState
{
    menu,
    battleHud,
    endRound,
    map,
}

public enum PopupState
{
    none,
    settings,
    levelInfo,
    collection,
    topbarExt,
    statShop,
    itemShop
}

public class PanelController : Singleton<PanelController>
{
    [Header("Panels")]
    public GameObject mainMenu;
    public GameObject gamePanel;
    public GameObject endRoundPanel;
    public GameObject mapPanel;    

    [Header("Popups")]
    public GameObject popup_settings;
    public GameObject popup_levelInfo;
    public GameObject popup_collection;
    public GameObject popup_topbarext;
    public GameObject popup_statshop;
    public GameObject popup_itemshop;

    [Header("Animators")]
    public Animator coinAnimator;
    public float coinTimer;

    public void SetPanelState(PageState state)
    {
        CloseAllPopups();
        DeactivateAllPanels();

        switch (state)
        {
            case PageState.battleHud:
                gamePanel.SetActive(true);
                break;
            case PageState.endRound:
                endRoundPanel.SetActive(true);
                break;
            case PageState.map:
                mapPanel.SetActive(true);
                break;
            case PageState.menu:
                mainMenu.SetActive(true);
                break;
        }
    }

    public void SetPopupState(PopupState state)
    {
        CloseAllPopups();
        BattleController.Instance.Pause();

        switch (state)
        {
            case PopupState.none:
                BattleController.Instance.UnPause();
                break;
            case PopupState.collection:
                popup_collection.SetActive(true);
                break;
            case PopupState.itemShop:
                popup_itemshop.SetActive(true);
                break;
            case PopupState.levelInfo:
                popup_levelInfo.SetActive(true);
                break;
            case PopupState.settings:
                popup_settings.SetActive(true);
                break;
            case PopupState.statShop:
                popup_statshop.SetActive(true);
                break;
            case PopupState.topbarExt:
                popup_topbarext.SetActive(true);
                break;
        }
    }

    public void GoToMainMenu()
    {
        DeactivateAllPanels();

        mainMenu.SetActive(true);
    }

    public void PlayCoinAnimation(int i)
    {
        StartCoroutine(CoinAnimation(i));
    }

    public IEnumerator CoinAnimation(int balance)
    {
        int coinsPerAnimation = 1;        

        while (balance > 0)
        {
            if (balance >= 5) coinsPerAnimation = 5;
            else if (balance < 5) coinsPerAnimation = 1;

            coinAnimator.SetTrigger("add");
            AudioController.Instance.PlayCoinSound();

            yield return new WaitForSeconds(coinTimer);
            GameDataController.Instance.data.basicStats.gold += coinsPerAnimation;
            balance -= coinsPerAnimation;            
        }
    }

    public void CloseAllPopups()
    {
        popup_levelInfo.SetActive(false);
        popup_settings.SetActive(false);
        popup_collection.SetActive(false);
        popup_topbarext.SetActive(false);
        popup_statshop.SetActive(false);
        popup_itemshop.SetActive(false);

        BattleController.Instance.UnPause();
        GameDataController.Instance.Save();
    }

    public void GoToStartGame()
    {
        DeactivateAllPanels();
        gamePanel.SetActive(true);        
    }

    public void OpenLevelInfoPopup()
    {
        CloseAllPopups();
        popup_levelInfo.SetActive(true);
    }

    public void OpenItemShopPopup()
    {
        CloseAllPopups();
        popup_itemshop.SetActive(true);
    }

    public void OpenStatShopPopup()
    {
        CloseAllPopups();
        popup_statshop.SetActive(true);
    }

    public void OpenSettingsPopup()
    {
        CloseAllPopups();        
        popup_settings.SetActive(true);
        BattleController.Instance.Pause();
    }

    public void OpenCollectionPopup()
    {
        CloseAllPopups();
        BattleController.Instance.Pause();
        popup_collection.SetActive(true);
    }

    public void OpenTopBarExtPopup()
    {
        CloseAllPopups();
        BattleController.Instance.Pause();
        popup_topbarext.SetActive(true);
    }

    public void GoToEndRound()
    {
        DeactivateAllPanels();
        endRoundPanel.SetActive(true);
    }
    public void GoToMap()
    {
        DeactivateAllPanels();

        mapPanel.SetActive(true);
        LevelManager.Instance.FillButtons();
    }

    public void DeactivateAllPanels()
    {
        CloseAllPopups();
        mainMenu.SetActive(false);
        endRoundPanel.SetActive(false);
        gamePanel.SetActive(false);
        mapPanel.SetActive(false);

        GameDataController.Instance.Save();
    }
}


