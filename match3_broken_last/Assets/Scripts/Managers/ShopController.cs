﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ShopController : Singleton<ShopController>
{
    [Header("HP objects")]
    public Text hpText;
    public Image hpImage;

    [Header("Atk objects")]
    public Text atkText;
    public Image atkImage;

    [Header("MAtk objects")]
    public Text matkText;
    public Image matkImage;

    [Header("Def objects")]
    public Text defText;
    public Image defImage;

    void OnEnable()
    {
        GameDataController.OnLoadGameData += OnLoadGameData;
    }

    void OnDisable()
    {
        GameDataController.OnLoadGameData -= OnLoadGameData;
    }

    private void OnLoadGameData()
    {
        Initialize();
    }

    public void Initialize()
    {
        SetButtons();
    }

    void SetButtons()
    {
        PlayerStats stats = GameDataController.Instance.data.basicStats;

        hpImage.sprite = stats.hp.sprite;
        hpText.text = (
                    "Buy " +
                    stats.hp.amountPerLevel + " " +
                    stats.hp.stat + " for " +
                    (stats.hp.GetPrice()) +
                    " gold");

        atkImage.sprite = stats.atk.sprite;
        atkText.text = (
                    "Buy " +
                    stats.atk.amountPerLevel + " " +
                    stats.atk.stat + " for " +
                    (stats.atk.GetPrice()) +
                    " gold");

        matkImage.sprite = stats.matk.sprite;
        matkText.text = (
                    "Buy " +
                    stats.matk.amountPerLevel + " " +
                    stats.matk.stat + " for " +
                    (stats.matk.GetPrice()) +
                    " gold");
        
        defImage.sprite = stats.def.sprite;
        defText.text = (
                    "Buy " +
                    stats.def.amountPerLevel + " " +
                    stats.def.stat + " for " +
                    (stats.def.GetPrice()) +
                    " gold");

    }  
}
