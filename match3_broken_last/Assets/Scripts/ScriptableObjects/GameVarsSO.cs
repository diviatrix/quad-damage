﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "GameVars", menuName = "ScriptableObjects/GameVars", order = 1)]
public class GameVarsSO : ScriptableObject
{
    public GameData gameData;
}
