﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EnemyInLevel
{
    public GameObject EnemyPrefab;
    public int amount;
}

[System.Serializable]
[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order = 1)]
public class LevelSO : ScriptableObject
{
    [Header("Bools")]
    public bool open;
    public bool repeatable;
    public bool enableBounty;

    [Header("Basic Settings")]
    public Sprite sprite;
    public Vector2 size;
    public string text;

    [Header("Enemy List")]
    public List<EnemyInLevel> enemies;    
}
