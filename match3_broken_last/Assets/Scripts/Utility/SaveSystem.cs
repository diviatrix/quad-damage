﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

[System.Serializable]
public struct SaveData
{
    public bool isEmpty;
    public string gameData;
}


public class SaveSystem : Singleton<SaveSystem>
{
    public void SaveGame(GameData gd)
    {
        SaveData saveData = new SaveData();
        saveData.gameData = JsonUtility.ToJson(gd);        

        WriteData(saveData);
    }

    public void WriteData(SaveData data)
    {
        // load file if exist
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);

        // save it
        bf.Serialize(file, data);
        file.Close();
    }    

    public SaveData LoadGameFromDisk()
    {
        Debug.Log("Start Loading");
        Debug.Log(Application.persistentDataPath);

        SaveData loadedData = new SaveData();

        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            
            loadedData = (SaveData)bf.Deserialize(file);
            loadedData.isEmpty = false;

            file.Close();
            Debug.Log("Data loaded from disk");
        }
        else
        {
            loadedData.isEmpty = true;
            Debug.Log("Save data not found on disk");
        }

        return loadedData;
    }
}
